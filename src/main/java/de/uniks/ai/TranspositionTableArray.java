package de.uniks.ai;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.BitSet;

/**
 * A TranspostionTable implementation based on an underlying array to store the entries. In case of a hash collision,
 * the 'greater' entry wins ({@code ComparatorTTEntry}). The hashing function works as follows and may be changed in
 * future: Board -> BitSet(122bit) -> MD5(160bit) -> int(32bit) -> index(%MaxSize)
 * 
 * The MD5 instance is used to create some diffusion in the index space, else there are too many indices that will never
 * be hit during a standard Abalone game.
 */
public class TranspositionTableArray<M extends MoveInterface<M>> {

    public TTEntry<M>[] tableData;

    // some attributes for internal statistics
    public int size = 0;

    public int collisions = 0;

    public int totalPuts = 0;

    private ComparatorTTEntry comperator = new ComparatorTTEntry();

    private MessageDigest md;

    private static final int MAX_SIZE = 1_000_000;

    @SuppressWarnings("unchecked")
    public TranspositionTableArray() {
        this.tableData = new TTEntry[MAX_SIZE];
        try {
            this.md = MessageDigest.getInstance("MD5");
        } catch(NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the entry to which the specified id is mapped.
     * 
     * @param id of the board
     * @param requiredDepth
     * @param imPlayerOne
     * @return The entry or null
     */
    public TTEntry<M> get(BitSet id, int requiredDepth, boolean imPlayerOne) {
        BigInteger bi = new BigInteger(this.md.digest(id.toByteArray()));
        int index = bi.intValue() % MAX_SIZE;
        index = (index < 0 ? index * -1 : index);
        TTEntry<M> entry = this.tableData[index];
        if(entry != null
                && entry.getId().equals(id)
                && entry.getDepth() >= requiredDepth
                && entry.isImPlayerOne() == imPlayerOne) {
            entry.increaseAccessCounter();
            return entry;
        }
        return null;
    }

    /**
     * Stores the specified entry this TranspositionTable.
     * 
     * @param entry
     */
    public void put(TTEntry<M> entry) {
        this.totalPuts++;
        BigInteger bi = new BigInteger(this.md.digest(entry.getId().toByteArray()));
        int index = bi.intValue() % MAX_SIZE;
        index = (index < 0 ? index * -1 : index);
        TTEntry<M> storedEntry = this.tableData[index];
        if(storedEntry != null) {
            this.collisions++;
            if(this.comperator.compare(entry, storedEntry) >= 1) {
                this.tableData[index] = entry;
            }
        } else {
            this.tableData[index] = entry;
            this.size++;
        }
    }
}
