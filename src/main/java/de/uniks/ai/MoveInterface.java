package de.uniks.ai;

public interface MoveInterface<M extends MoveInterface<M>> {

    public boolean equals(M move);

    public int compareTo(M move);

    public boolean hitMove();
}
