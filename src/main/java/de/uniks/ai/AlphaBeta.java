package de.uniks.ai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

public class AlphaBeta<L extends LogicInterface<B, M, P>, B extends BoardInterface, M extends MoveInterface<M>, P extends PlayerInterface>
        extends AI<L, B, M, P> {

    private boolean traTableActive = true;

    public void setTraTableActive(boolean isActive) {
        traTableActive = isActive;
    }

    public int entriesUsed;

    public TranspositionTableHM<M> tt = new TranspositionTableHM<>();

    // ==========================================================================
    private Strategy<M> bestStrategy;

    /**
     * getBestStrategy
     * 
     * @return
     */
    public Strategy<M> getBestStrategy() {
        return bestStrategy;
    }

    public void setBestStrategy(Strategy<M> bestStrategy) {
        this.bestStrategy = bestStrategy;
    }

    // ==========================================================================
    private int desiredDepth;

    public int getDesiredDepth() {
        return this.desiredDepth;
    }

    public void setDesiredDepth(int value) {
        if(this.desiredDepth != value) {
            this.desiredDepth = value;
        }
    }

    public AlphaBeta<L, B, M, P> withDesiredDepth(int value) {
        setDesiredDepth(value);
        return this;
    }

    // ==========================================================================
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(" ").append(this.getDesiredDepth());
        return result.substring(1);
    }

    // ==========================================================================
    private P maxPlayer;

    public P getMaxPlayer() {
        return this.maxPlayer;
    }

    public void setMaxPlayer(P value) {
        if(this.playerOne == null) {
            this.playerOne = value;
        }
        if(this.maxPlayer != value) {
            this.maxPlayer = value;
        }
    }

    public AlphaBeta<L, B, M, P> withMaxPlayer(P value) {
        setMaxPlayer(value);
        return this;
    }

    // ==========================================================================
    private P minPlayer;

    public P getMinPlayer() {
        return this.minPlayer;
    }

    public void setMinPlayer(P value) {
        if(this.minPlayer != value) {
            this.minPlayer = value;
        }
    }

    public AlphaBeta<L, B, M, P> withMinPlayer(P value) {
        setMinPlayer(value);
        return this;
    }

    // ==========================================================================
    private P playerOne;

    public boolean imPlayerOne(int i) {
        if(i == 1) {
            return(getMaxPlayer() == this.playerOne);
        }
        if(i == -1) {
            return(getMinPlayer() == this.playerOne);
        }
        return false;
    }

    // ==========================================================================
    public AlphaBeta() {
        // console output
        getStatusString().addListener((arg) -> {
            System.out.println(arg);
        });
    }

    /**
     * Optimized version of the classic recursive minimax algorithm. Cuts off specific parts of the search tree to speed
     * up the process. Calculates the best possible move for a given board. The best possible strategy can be accessed
     * with the getBestStrategy method.
     * 
     * @param logic is an object of the Logic class.
     * @param playerMax Is an object of the Player class. Used to set the maximizing player.
     * @param playerMin Is an object of the Player class. Used to set the minimizing player.
     * @param board Is an object of the Board class.
     * @param depth is of type integer. Determines the maximum search depth.
     * 
     * @return move returns the best possible move for the given depth.
     */
    @Override
    public M calculateBestMove(L logic, P playerMax, P playerMin, B board, int depth) {

        // Sets up the maximizing, minimizing player and the maximum search
        // depth.
        this.setDesiredDepth(depth);
        this.setMaxPlayer(playerMax);
        this.setMinPlayer(playerMin);

        Timer timer = createTimerForSpeedString();
        // Starts the algorithm
        // Start value for alpha is Integer.MIN_VALUE + 1, for beta
        // Integer.MAX_VALUE.
        Strategy<M> strategy = alphaBeta(logic, board, depth, 1, Integer.MIN_VALUE + 1, Integer.MAX_VALUE, false);
        setBestStrategy(strategy);
        timer.cancel();

        if(strategy != null) {
            for(M m: strategy.getMoveList()) {
                System.out.println(m.toString());
            }
        } else {
            return null;
        }
        return strategy.getMoveList().get(0);
        // int score = alphaBetaInt(logic, board, depth, 1, -1000, 1000);
        // return optimalMove;
    }

    private int evaluate(L logic, B board, P player, int depth) {
        return logic.evaluate(board, player) - depth;
    }

    /*
     * Alphabeta algorithm as negamax using a transposition tables, presorting, quiescence search
     */
    private Strategy<M> alphaBeta(L logic, B board, int depth, int player, int alpha, int beta, boolean wasHitMove) {

        setVisitedNodes(getVisitedNodes() + 1);

        // Get all possible moves from the minimizing or maximizing player
        ArrayList<M> allMoves = null;
        if(player == 1) {
            allMoves = logic.getAllMoves(this.maxPlayer, board);
        } else if(player == -1) {
            allMoves = logic.getAllMoves(this.minPlayer, board);
        }

        // Evaluate the board, if there are no more possibles moves or there is
        // a winner,
        // or if the maximum depth is reached and the last move wasn't a hit
        // move.
        if((depth <= 0 && !wasHitMove) || allMoves.size() == 0 || logic.checkWinner(board) != null) {
            Strategy<M> strategy = new Strategy<>();
            int score = 0;
            if(player == 1) {
                score = evaluate(logic, board, this.minPlayer, depth);
            } else if(player == -1) {
                score = evaluate(logic, board, this.maxPlayer, depth);
            }
            strategy.setScore(score);
            return strategy;
        }
        // Presorting the allMoves arraylist to optimize the algorithms runtime.
        Collections.sort(allMoves, (m1, m2) -> m1.compareTo(m2));

        int maxValue = Integer.MIN_VALUE + 1;
        Strategy<M> bestStrategy = null;

        // Go through all possible moves.
        // Execute move, go one depth deeper and undo the move. Prevents a
        // memory overflow.
        for(int i = 0; i < allMoves.size(); i++) {
            if(getStopFlag()) {
                return null;
            }
            M move = allMoves.get(i);
            if(depth == this.desiredDepth) {
                setStatusStringValue(
                        "Simulate move " + (i + 1) + "/" + allMoves.size() + " | " + ((i + 1) * 100 / allMoves.size())
                                + "% done. \t\t\t Strategy: " + (bestStrategy != null ? bestStrategy.toString() : "")
                                + (bestStrategy != null ? (" with Score " + bestStrategy.getScore()) : ""));
            }
            // Simulate move.
            logic.executeMoveSimulation(move);

            Strategy<M> returnedStrategy;
            if(traTableActive) {
                TTEntry<M> entry = this.tt.get(board.encode(), depth, imPlayerOne(player));
                if(entry != null) {
                    returnedStrategy = entry.getStrategyCopy();
                    int factor = player * entry.getPlayer();
                    returnedStrategy.setScore(returnedStrategy.getScore() * factor);
                    entriesUsed++;
                } else {
                    returnedStrategy = alphaBeta(logic, board, depth - 1, -player, -beta, -alpha, move.hitMove());
                    if(returnedStrategy != null) {
                        returnedStrategy.setScore(returnedStrategy.getScore() * -1);
                        this.tt.put(
                                new TTEntry<>(board.encode(), returnedStrategy, depth, imPlayerOne(player), player));
                    }
                }
            } else {
                // Call the recursive function with the new board situation.
                returnedStrategy = alphaBeta(logic, board, depth - 1, -player, -beta, -alpha, move.hitMove());
                // Added the calculated score to the calculated strategy object.
                if(returnedStrategy != null) {
                    returnedStrategy.setScore(returnedStrategy.getScore() * -1);
                }
            }
            // Undo the simulated move.
            logic.undoMoveSimulation(move);
            // Update the new calculated strategy, if it has a better score.
            if(returnedStrategy != null && returnedStrategy.getScore() > maxValue) {
                maxValue = returnedStrategy.getScore();
                // Add move to the strategy
                returnedStrategy.getMoveList().add(0, move);
                bestStrategy = returnedStrategy;
                if(depth == this.desiredDepth) {
                    setStatusStringValue("Simulate move " + (i + 1) + "/" + allMoves.size() + " | "
                            + ((i + 1) * 100 / allMoves.size()) + "% done. \t\t\t Strategy: "
                            + (bestStrategy != null ? bestStrategy.toString() : "")
                            + (bestStrategy != null ? (" with Score " + bestStrategy.getScore()) : ""));
                }
            }
            // Update alpha value if there is a better move.
            if(maxValue > alpha) {
                alpha = maxValue;
            }
            // Cut off, the current score is better than the beta value.
            if(maxValue >= beta) {
                // System.out.println("schnipp schnapp zweig ab");
                break;
            }
        }

        // Set the status string, if the desired depth is reached.
        if(depth == this.desiredDepth) {
            setStatusStringValue(
                    "AI choosed strategy " + bestStrategy.toString() + "  with score " + bestStrategy.getScore());
        }
        return bestStrategy;
    }

    private Timer createTimerForSpeedString() {
        long startTime = System.currentTimeMillis();
        class StringTimer extends TimerTask {
            public void run() {
                String elapsedTimeString = "elapsed time: " + ((System.currentTimeMillis() - startTime) / 1000)
                        + " seconds";
                String speedString = "speed: " + getVisitedNodes() + " Nodes/s";

                getSpeedString().setValue(speedString + "\t\t\t " + elapsedTimeString);
                setVisitedNodes(0);
            }
        }
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new StringTimer(), 0, 1000);
        return timer;
    }
}
