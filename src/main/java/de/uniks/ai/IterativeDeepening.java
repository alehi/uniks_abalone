package de.uniks.ai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

public class IterativeDeepening<L extends LogicInterface<B, M, P>, B extends BoardInterface, M extends MoveInterface<M>, P extends PlayerInterface>
        extends AI<L, B, M, P> {

    // ==========================================================================

    public static final long NO_TIME_LIMIT = 0;

    private long timeLimit = NO_TIME_LIMIT;

    /**
     * IterativeDeepening getTimeLimit
     * 
     * @return
     */
    public long getTimeLimit() {
        return timeLimit;
    }

    /**
     * IterativeDeepening setTimeLimit
     * 
     * @param timeLimit
     */
    public void setTimeLimit(long timeLimit) {
        this.timeLimit = timeLimit;
    }

    // ==========================================================================

    private boolean traTableActive = true;

    /**
     * IterativeDeepening setTraTableActive
     * 
     * @param isActive
     */
    public void setTraTableActive(boolean isActive) {
        traTableActive = isActive;
    }

    public int entriesUsed;

    public TranspositionTableHM<M> tt = new TranspositionTableHM<>();

    // ==========================================================================

    private Strategy<M> iterativeDeepeningStrategy;

    /**
     * IterativeDeepening getIterativeDeepeningStrategy
     * 
     * @return
     */
    public Strategy<M> getIterativeDeepeningStrategy() {
        return iterativeDeepeningStrategy;
    }

    /**
     * IterativeDeepening setIterativeDeepeningStrategy
     * 
     * @param iterativeDeepeningStrategy
     */
    public void setIterativeDeepeningStrategy(Strategy<M> iterativeDeepeningStrategy) {
        this.iterativeDeepeningStrategy = iterativeDeepeningStrategy;
    }

    // ==========================================================================
    private int desiredDepth;

    /**
     * IterativeDeepening getDesiredDepth
     * 
     * @return
     */
    public int getDesiredDepth() {
        return this.desiredDepth;
    }

    /**
     * IterativeDeepening withDesiredDepth
     * 
     * @param value
     */
    public void setDesiredDepth(int value) {
        if(this.desiredDepth != value) {
            this.desiredDepth = value;
        }
    }

    /**
     * IterativeDeepening withDesiredDepth
     * 
     * @param value
     * @return
     */
    public IterativeDeepening<L, B, M, P> withDesiredDepth(int value) {
        setDesiredDepth(value);
        return this;
    }

    // ==========================================================================
    @Override
    /**
     * IterativeDeepening toString
     */
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(" ").append(this.getDesiredDepth());
        return result.substring(1);
    }

    // ==========================================================================
    private P maxPlayer;

    /**
     * IterativeDeepening getMaxPlayer
     * 
     * @return
     */
    public P getMaxPlayer() {
        return this.maxPlayer;
    }

    /**
     * IterativeDeepening setMaxPlayer
     * 
     * @param value
     */
    public void setMaxPlayer(P value) {
        if(this.playerOne == null) {
            this.playerOne = value;
        }
        if(this.maxPlayer != value) {
            this.maxPlayer = value;
        }
    }

    /**
     * IterativeDeepening withMaxPlayer
     * 
     * @param value
     * @return
     */
    public IterativeDeepening<L, B, M, P> withMaxPlayer(P value) {
        setMaxPlayer(value);
        return this;
    }

    // ==========================================================================
    private P minPlayer;

    /**
     * IterativeDeepening getMinPlayer
     * 
     * @return
     */
    public P getMinPlayer() {
        return this.minPlayer;
    }

    /**
     * IterativeDeepening setMinPlayer
     * 
     * @param value
     */
    public void setMinPlayer(P value) {
        if(this.minPlayer != value) {
            this.minPlayer = value;
        }
    }

    /**
     * IterativeDeepening withMinPlayer
     * 
     * @param value
     * @return
     */
    public IterativeDeepening<L, B, M, P> withMinPlayer(P value) {
        setMinPlayer(value);
        return this;
    }

    // ==========================================================================
    private P playerOne;

    /**
     * IterativeDeepening imPlayerOne
     * 
     * @param i
     * @return
     */
    public boolean imPlayerOne(int i) {
        if(i == 1) {
            return(getMaxPlayer() == this.playerOne);
        }
        if(i == -1) {
            return(getMinPlayer() == this.playerOne);
        }
        return false;
    }

    // ==========================================================================

    /**
     * used by statusString
     */
    private String depthString = new String();

    /**
     * IterativeDeepening getDepthString
     * 
     * @return depthString
     */
    public String getDepthString() {
        return depthString;
    }

    /**
     * IterativeDeepening setDepthString
     * 
     * @param depthString
     */
    public void setDepthString(String depthString) {
        this.depthString = depthString;
    }

    // ==========================================================================

    /**
     * IterativeDeepening IterativeDeepening
     */
    public IterativeDeepening() {
        // console output
        getStatusString().addListener((arg) -> {
            System.out.println(arg);
        });
    }

    private int evaluate(L logic, B board, P player, int depth) {
        return logic.evaluate(board, player) - depth;
    }

    /**
     * Special version of the alphabeta algorithm. Calculates depth one for all moves, then depth two for all moves and
     * so on. If the algorithm is canceled during the calculation, it still returns a usable move.
     * 
     * @param logic is an object of the Logic class.
     * @param playerMax Is an object of the Player class. Used to set the maximizing player.
     * @param playerMin Is an object of the Player class. Used to set the minimizing player.
     * @param board Is an object of the Board class.
     * @param depth is of type integer. Determines the maximum search depth.
     * 
     * @return move returns the best possible move for the given depth.
     */
    @SuppressWarnings("rawtypes")
    public M calculateBestMove(L logic, P playerMax, P playerMin, B board, int depth) {

        // Sets up the maximizing and minimizing player
        this.setMaxPlayer(playerMax);
        this.setMinPlayer(playerMin);

        M bestMoveSoFar = logic.getAllMoves(playerMax, board).get(0);
        Strategy<M> currentStrategy = null;

        // start Timer for speedString, updates every second
        Timer timer = createTimerForSpeedString();

        // create timerThread to stop the AI when the timiLimit is reached
        if(timeLimit != NO_TIME_LIMIT) {
            AI me = this;
            new Thread("IterativeDTimerThread") {
                @Override
                public void run() {
                    try {
                        Thread.sleep(timeLimit);
                        me.stopAiThread();
                    } catch(InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }

        // Starts algorithm at depth 1.
        for(int i = 1; i <= depth; i++) {

            this.setDesiredDepth(i);

            currentStrategy = alphaBetaIterativ(logic, board, i, 1, Integer.MIN_VALUE + 1, Integer.MAX_VALUE, false);
            if(getStopFlag())
                break;
            bestMoveSoFar = currentStrategy.getMoveList().get(0);
            setIterativeDeepeningStrategy(currentStrategy);

            setDepthString("Analizing depth " + i);
            updateStatusString();

        }

        // reset the stopFlag
        setStopFlag(false);

        // cancel the speedStringTimer
        timer.cancel();
        return bestMoveSoFar;
    }

    private Strategy<M> alphaBetaIterativ(L logic, B board, int depth, int player, int alpha, int beta,
            boolean wasHitMove) {

        setVisitedNodes(getVisitedNodes() + 1);

        // Get all possible moves from the minimizing or maximizing player
        ArrayList<M> allMoves = null;
        if(player == 1) {
            allMoves = logic.getAllMoves(this.maxPlayer, board);
        } else if(player == -1) {
            allMoves = logic.getAllMoves(this.minPlayer, board);
        }

        // Evaluate the board, if there are no more possibles moves or there is
        // a winner,
        // or if the maximum depth is reached and the last move wasn't a hit
        // move.
        if((depth <= 0 && !wasHitMove) || allMoves.size() == 0 || logic.checkWinner(board) != null) {
            Strategy<M> strategy = new Strategy<>();
            int score = 0;
            if(player == 1) {
                score = evaluate(logic, board, this.minPlayer, depth);
            } else if(player == -1) {
                score = evaluate(logic, board, this.maxPlayer, depth);
            }
            strategy.setScore(score);
            return strategy;
        }

        // Analyze the best move of the previous iteration first, instead of
        // normal sort of the arraylist
        if(depth == getDesiredDepth()) {
            if(getDesiredDepth() > 1) {
                M bestInitialMove = getIterativeDeepeningStrategy().getMoveList().get(0);
                int indexOfDuplicateMove = 0;
                for(M m: allMoves) {
                    if(m.equals(bestInitialMove)) {
                        indexOfDuplicateMove = allMoves.indexOf(m);
                        break;
                    }
                }
                allMoves.remove(indexOfDuplicateMove);
                allMoves.add(0, bestInitialMove);
            } else {
                Collections.sort(allMoves, (m1, m2) -> m1.compareTo(m2));
            }
        } else {
            Collections.sort(allMoves, (m1, m2) -> m1.compareTo(m2));
        }
        // Collections.sort(allMoves, (m1, m2) -> m1.compareTo(m2));
        int maxValue = Integer.MIN_VALUE + 1;
        Strategy<M> bestStrategy = null;

        // Go through all possible moves.
        // Execute move, go one depth deeper and undo the move. Prevents a
        // memory overflow.
        for(int i = 0; i < allMoves.size(); i++) {
            if(getStopFlag()) {
                return null;
            }
            M move = allMoves.get(i);

            // Simulate move.
            logic.executeMoveSimulation(move);

            Strategy<M> returnedStrategy;
            if(traTableActive) {
                TTEntry<M> entry = this.tt.get(board.encode(), depth, imPlayerOne(player));
                if(entry != null) {
                    returnedStrategy = entry.getStrategyCopy();
                    int factor = player * entry.getPlayer();
                    returnedStrategy.setScore(returnedStrategy.getScore() * factor);
                    entriesUsed++;
                } else {
                    returnedStrategy = alphaBetaIterativ(logic, board, depth - 1, -player, -beta, -alpha,
                            move.hitMove());
                    if(returnedStrategy != null) {
                        returnedStrategy.setScore(returnedStrategy.getScore() * -1);
                        this.tt.put(
                                new TTEntry<>(board.encode(), returnedStrategy, depth, imPlayerOne(player), player));
                    }
                }
            } else {
                // Call the recursive function with the new board situation.
                returnedStrategy = alphaBetaIterativ(logic, board, depth - 1, -player, -beta, -alpha, move.hitMove());
                // Added the calculated score to the calculated strategy object.
                if(returnedStrategy != null) {
                    returnedStrategy.setScore(returnedStrategy.getScore() * -1);
                }
            }
            // Undo the simulated move.
            logic.undoMoveSimulation(move);

            // Update the new calculated strategy, if it has a better score.
            if(returnedStrategy != null && returnedStrategy.getScore() > maxValue) {
                maxValue = returnedStrategy.getScore();
                // Add move to the strategy
                returnedStrategy.getMoveList().add(0, move);
                bestStrategy = returnedStrategy;

                if(depth == this.desiredDepth) {
                    updateStatusString();
                }
            }
            // Update alpha value if there is a better move.
            if(maxValue > alpha) {
                alpha = maxValue;
            }
            // Cut off, the current score is better than the beta value.
            if(maxValue >= beta) {
                // System.out.println("schnipp schnapp zweig ab");
                break;
            }
        }
        return bestStrategy;
    }

    private void updateStatusString() {
        setStatusStringValue(getDepthString() + " | "
                + (getIterativeDeepeningStrategy() != null ? getIterativeDeepeningStrategy().toString() : "")
                + (getIterativeDeepeningStrategy() != null
                        ? (" with Score " + getIterativeDeepeningStrategy().getScore()) : ""));

    }

    private Timer createTimerForSpeedString() {
        long startTime = System.currentTimeMillis();
        class StringTimer extends TimerTask {
            public void run() {
                String elapsedTimeString = "elapsed time: " + ((System.currentTimeMillis() - startTime) / 1000)
                        + " seconds";
                String speedString = "speed: " + getVisitedNodes() + " Nodes/s";

                getSpeedString().setValue(speedString + "\t\t\t " + elapsedTimeString);
                setVisitedNodes(0);
            }
        }
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new StringTimer(), 0, 1000);
        return timer;
    }

}
