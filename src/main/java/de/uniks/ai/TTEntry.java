package de.uniks.ai;

import java.util.ArrayList;
import java.util.BitSet;

public class TTEntry<M extends MoveInterface<M>> {

    /**
     * The encoded value of a specific board
     */
    private BitSet id;

    public BitSet getId() {
        return id;
    }

    public void setId(BitSet id) {
        this.id = id;
    }

    // ==========================================================================
    private int depth;

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    // ==========================================================================
    private int accessCounter = 0;

    public int getAccessCounter() {
        return accessCounter;
    }

    public void setAccessCounter(int accessCounter) {
        this.accessCounter = accessCounter;
    }

    public void increaseAccessCounter() {
        this.accessCounter++;
    }

    // ==========================================================================
    /**
     * Indicates if this board belongs to playerOne
     */
    private boolean imPlayerOne;

    public boolean isImPlayerOne() {
        return imPlayerOne;
    }

    public void setImPlayerOne(boolean imPlayerOne) {
        this.imPlayerOne = imPlayerOne;
    }

    // ==========================================================================
    /**
     * Indicates if the score is from a min(-) or max(+) player.
     */
    private int player;

    public int getPlayer() {
        return player;
    }

    public void setPlayer(int player) {
        this.player = player;
    }

    // ==========================================================================
    // disable default constructor
    @SuppressWarnings("unused")
    private TTEntry() {
    };

    public TTEntry(BitSet id, Strategy<M> strategy, int depth, boolean imPlayerOne, int player) {
        this.id = (BitSet) id.clone();
        // create a copy of that object
        Strategy<M> strategyCopy = new Strategy<M>();
        strategyCopy.setScore(strategy.getScore());
        strategyCopy.setMoveList(new ArrayList<M>());
        this.strategy = strategyCopy;
        this.depth = depth;
        this.setImPlayerOne(imPlayerOne);
        this.setPlayer(player);
    }

    private Strategy<M> strategy;

    public Strategy<M> getStrategyCopy() {
        // create a copy of that object
        Strategy<M> strategyCopy = new Strategy<M>();
        strategyCopy.setScore(strategy.getScore());
        // empty move list, because we cant ensure that these moves are still valid
        strategyCopy.setMoveList(new ArrayList<M>());
        return strategyCopy;
    }

    public void setStrategy(Strategy<M> strategy) {
        this.strategy = strategy;
    }

}
