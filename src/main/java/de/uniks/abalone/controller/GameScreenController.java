package de.uniks.abalone.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Optional;

import de.uniks.abalone.gui.InfoDialog;
import de.uniks.abalone.gui.RulesDialog;
import de.uniks.abalone.gui.SettingsDialog;
import de.uniks.abalone.model.AIPlayer;
import de.uniks.abalone.model.Board;
import de.uniks.abalone.model.Field;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Logic;
import de.uniks.abalone.model.Move;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.model.util.GameCreator;
import de.uniks.abalone.tools.Layouts;
import de.uniks.ai.AI;
import de.uniks.ai.AlphaBeta;
import de.uniks.ai.IterativeDeepening;
import de.uniks.ai.MiniMax;
import de.uniks.networkparser.IdMap;
import de.uniks.networkparser.ext.javafx.window.FXStageController;
import de.uniks.networkparser.ext.javafx.window.StageEvent;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * The {@code GameScreenController} provides the whole functionality for the game screen. It creates the gui elements
 * for the game, connects the gui with the data model and visualizes the game progress.
 */
public class GameScreenController implements StageEvent {
    private Stage stage;

    private FXStageController controller;

    private Double boardWidth;

    private Group boardgroup;

    private Game game;

    private int mode;

    private boolean logfile;

    private BufferedWriter logger;

    @FXML
    private MenuItem mn_changeAI;

    @FXML
    private MenuItem mn_saveGame;

    @FXML
    private Label lbl_name1;

    @FXML
    private Label lbl_name2;

    @FXML
    private Pane pn_board;

    @FXML
    private ListView<HistoryEntry> history_list;

    @FXML
    private Label ai_status_text;

    @FXML
    private Label ai_speed_text;

    public FXStageController getController() {
        return this.controller;
    }

    public Group getBoardGroup() {
        return this.boardgroup;
    }

    public Double getBoardWidth() {
        return this.boardWidth;
    }

    public MenuItem getSaveMenuItem() {
        return this.mn_saveGame;
    }

    /**
     * This method gets called after showing the stage or by the 'Restart Game' menu. It creates gui nodes for every
     * object and connects them to the data model.
     *
     * @param game The data mode of the 'Abalone' game
     * @param mode The initial token layout
     * @param logfile The flag for creating a log file or not
     */
    public void initStage(Game game, int mode, boolean logfile) {
        this.game = game;
        this.mode = mode;
        this.logfile = logfile;

        // General settings
        Image icon = new Image(this.getClass().getResource("/gfx/icon.png").toString());
        this.stage.getIcons().add(icon);
        Player player1 = game.getPlayers().get(0);
        Player player2 = game.getPlayers().get(1);
        this.stage.setTitle("Abalone - " + player1.getName() + " vs. " + player2.getName());
        this.stage.setHeight(Screen.getPrimary().getVisualBounds().getHeight());
        this.stage.setWidth(Screen.getPrimary().getVisualBounds().getWidth());
        this.stage.centerOnScreen();

        // It is possible to interrupt the IterativeDeepening AI with pressing the space bar
        this.stage.addEventHandler(KeyEvent.ANY, event -> {
            if(event.getCode().equals(KeyCode.SPACE)) {
                Player player = this.game.getCurrentPlayer();
                if(player instanceof AIPlayer) {
                    if(((AIPlayer) player).getAi() instanceof IterativeDeepening) {
                        ((AIPlayer) player).getAi().stopAiThread();
                    }
                }
            }
        });

        // Prepare the player labels and score token
        this.lbl_name1.setText(player1.getName());
        this.lbl_name2.setText(player2.getName());
        if(this.game.getCurrentPlayer().equals(player1)) {
            this.lbl_name1.setUnderline(true);
            this.lbl_name2.setUnderline(false);
        } else {
            this.lbl_name1.setUnderline(false);
            this.lbl_name2.setUnderline(true);
        }
        for(int i = 1; i <= 6; i++) {
            Circle token1 = (Circle) this.controller.getElementById("#player1_" + i);
            token1.setFill(Color.web(player2.getColor()));
            token1.setVisible(false);
            Circle token2 = (Circle) this.controller.getElementById("#player2_" + i);
            token2.setFill(Color.web(player1.getColor()));
            token2.setVisible(false);
        }

        // Draw the board
        this.boardgroup = new Group();
        Polygon board = new Polygon();
        board.setFill(new ImagePattern(new Image("/gfx/wood.jpg")));
        board.setStroke(Color.BLACK);
        board.setStrokeWidth(3);
        this.boardgroup.getChildren().add(board);
        Double paneWidth = this.pn_board.getWidth();
        Double paneHeight = this.pn_board.getHeight();
        this.boardWidth = paneWidth <= (paneHeight * Math.sqrt(3) / 4) ? paneWidth : (2 * paneHeight / Math.sqrt(3));
        Double boardHeight = this.boardWidth * Math.sqrt(3) / 2;
        board.getPoints().addAll(
                0.25 * this.boardWidth, 0.0,            // top left corner
                0.75 * this.boardWidth, 0.0,            // top right corner
                this.boardWidth, 0.5 * boardHeight,     // right corner
                0.75 * this.boardWidth, boardHeight,    // bottom right corner
                0.25 * this.boardWidth, boardHeight,    // bottom left corner
                0., 0.5 * boardHeight);                 // left corner
        this.boardgroup.relocate((paneWidth - this.boardWidth) / 2, (paneHeight - boardHeight) / 2);
        this.pn_board.getChildren().add(this.boardgroup);
        if(this.game.getCurrentPlayer() instanceof AIPlayer) {
            this.mn_saveGame.setDisable(true);
        }

        // Set up all controller and listener
        for(Field field: game.getFields()) {
            new FieldController(field, this);
        }
        new GameController(game, this.controller, this);
        new HistoryController(this.history_list, game);

        // Register listener for the AI status strings and the automated move execution
        for(Player player: game.getPlayers()) {
            if(player instanceof AIPlayer) {
                AIPlayer aiplayer = (AIPlayer) player;
                // status string
                aiplayer.getAi().getStatusString()
                        .addListener((ChangeListener<String>) (arg0, oldValue, newValue) -> Platform
                                .runLater(() -> GameScreenController.this.ai_status_text.setText(newValue)));
                aiplayer.getAi().getSpeedString()
                        .addListener((ChangeListener<String>) (arg0, oldValue, newValue) -> Platform
                                .runLater(() -> GameScreenController.this.ai_speed_text.setText(newValue)));
                // currentGame listener for automated move execution
                aiplayer.registerCurrentGameListener();
            }
        }

        // since the currentPlayer asso was set before the listener was registered, we have to initiate the automated
        // execution process
        if(game.getCurrentPlayer() instanceof AIPlayer) {
            ((AIPlayer) game.getCurrentPlayer()).doYourMove();
        }

        // If selected, create a logger for the game logic
        if(this.logfile) {
            String file = "";
            try {
                file = URLDecoder.decode(this.getClass().getResource("/logs").getPath(), "UTF-8");
            } catch(UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            // Filename = YYYYMMDD_HHmmSS.log
            Calendar date = Calendar.getInstance();
            DecimalFormat df = new DecimalFormat("00");
            file += "/";
            file += date.get(Calendar.YEAR);
            file += df.format(date.get(Calendar.MONTH + 1));
            file += df.format(date.get(Calendar.DAY_OF_MONTH));
            file += "_";
            file += df.format(date.get(Calendar.HOUR_OF_DAY));
            file += df.format(date.get(Calendar.MINUTE));
            file += df.format(date.get(Calendar.SECOND));
            file += ".log";
            try {
                this.logger = new BufferedWriter(new FileWriter(file, true));
            } catch(IOException e) {
                e.printStackTrace();
            }
            this.game.getLogic().setLogger(this.logger);

            // Player informations
            // Player 1
            StringBuilder entry = new StringBuilder();
            entry.append("Player 1 - ");
            if(player1 instanceof AIPlayer) {
                entry.append(((AIPlayer) player1).getAi().getClass().getSimpleName() + "-AI" + System.lineSeparator());
                entry.append("Level: ");
                if(((AIPlayer) player1).getAi() instanceof IterativeDeepening) {
                    entry.append(((IterativeDeepening<Logic, Board, Move, Player>) ((AIPlayer) player1).getAi())
                            .getTimeLimit() / 1000);
                } else {
                    entry.append(((AIPlayer) player1).getStrength());
                }
                entry.append(System.lineSeparator());
            } else {
                entry.append("Human player" + System.lineSeparator());
            }
            entry.append("Name: " + player1.getName() + System.lineSeparator());
            entry.append(System.lineSeparator());

            // Player 2
            entry.append("Player 2 - ");
            if(player2 instanceof AIPlayer) {
                entry.append(((AIPlayer) player2).getAi().getClass().getSimpleName() + "-AI" + System.lineSeparator());
                entry.append("Level: ");
                if(((AIPlayer) player2).getAi() instanceof IterativeDeepening) {
                    entry.append(((IterativeDeepening<Logic, Board, Move, Player>) ((AIPlayer) player2).getAi())
                            .getTimeLimit() / 1000);
                } else {
                    entry.append(((AIPlayer) player2).getStrength());
                }
                entry.append(System.lineSeparator());
            } else {
                entry.append("Human player" + System.lineSeparator());
            }
            entry.append("Name: " + player2.getName() + System.lineSeparator());
            entry.append(System.lineSeparator());

            // Moves
            entry.append("Moves");
            entry.append(System.lineSeparator());

            // Write it to the log file
            try {
                this.logger.write(entry.toString());
                this.logger.flush();
            } catch(IOException e) {
                e.printStackTrace();
            }
        }

        // At the beginning the rules dialog is displayed
        showRules();
    }

    /**
     * This method gets called by the 'Restart Game' menu.
     * <p>
     * With exact copies of the old players we initialize a new {@code Game} object and simply pass it again to the
     * {@code GameScreenController}.
     */
    public void restartGame() {
        // Interrupt the AI thread and close the logger
        killAIThread();
        closeWriter();

        // Copy the old player objects
        Player player1 = null, player2 = null;

        // Player 1
        Player oldPlayer1 = this.game.getPlayers().first();
        if(oldPlayer1 instanceof AIPlayer) {
            player1 = new AIPlayer().withStrength(((AIPlayer) oldPlayer1).getStrength());
            if(((AIPlayer) oldPlayer1).getAi() instanceof MiniMax) {
                AI<Logic, Board, Move, Player> ai = new MiniMax<>();
                ((AIPlayer) player1).withAi(ai);
            } else if(((AIPlayer) oldPlayer1).getAi() instanceof AlphaBeta) {
                AI<Logic, Board, Move, Player> ai = new AlphaBeta<>();
                ((AIPlayer) player1).withAi(ai);
            } else {
                AI<Logic, Board, Move, Player> ai = new IterativeDeepening<>();
                long timeLimit = ((IterativeDeepening<Logic, Board, Move, Player>) ((AIPlayer) oldPlayer1).getAi())
                        .getTimeLimit();
                ((IterativeDeepening<Logic, Board, Move, Player>) ai).setTimeLimit(timeLimit);
                ((AIPlayer) player1).withAi(ai);
            }
        } else {
            player1 = new Player();
        }
        player1.withName(oldPlayer1.getName()).withColor(oldPlayer1.getColor());

        // Player 2
        Player oldPlayer2 = oldPlayer1.getNext();
        if(oldPlayer2 instanceof AIPlayer) {
            player2 = new AIPlayer().withStrength(((AIPlayer) oldPlayer2).getStrength());
            if(((AIPlayer) oldPlayer2).getAi() instanceof MiniMax) {
                AI<Logic, Board, Move, Player> ai = new MiniMax<>();
                ((AIPlayer) player2).withAi(ai);
            } else if(((AIPlayer) oldPlayer2).getAi() instanceof AlphaBeta) {
                AI<Logic, Board, Move, Player> ai = new AlphaBeta<>();
                ((AIPlayer) player2).withAi(ai);
            } else {
                AI<Logic, Board, Move, Player> ai = new IterativeDeepening<>();
                long timeLimit = ((IterativeDeepening<Logic, Board, Move, Player>) ((AIPlayer) oldPlayer2).getAi())
                        .getTimeLimit();
                ((IterativeDeepening<Logic, Board, Move, Player>) ai).setTimeLimit(timeLimit);
                ((AIPlayer) player2).withAi(ai);
            }
        } else {
            player2 = new Player();
        }
        player2.withName(oldPlayer2.getName()).withColor(oldPlayer2.getColor());

        // Create a new game object
        this.game = new Game();
        if(this.mode == 0) {
            this.game.init(player1, player2, Layouts.DEFAULT_TOKEN_LAYOUT);
        } else {
            this.game.init(player1, player2, Layouts.BELGIAN_DAISY_TOKEN_LAYOUT);
        }

        // Re-initialize the stage with the new model
        initStage(this.game, this.mode, this.logfile);
    }

    /**
     * This method gets called by the 'Start New Game' menu.
     * <p>
     * It interrupts the AI thread, closes the logger and the game screen and displays the start screen.
     */
    public void startNewGame() {
        killAIThread();
        closeWriter();
        this.controller.showNewStage("/fxml/StartScreen.fxml", this.getClass());
    }

    /**
     * This method gets called by the 'Exit Game' menu.
     * <p>
     * It interrupts the AI thread, closes the logger and shut down the program.
     */
    public void exitGame() {
        killAIThread();
        closeWriter();
        System.exit(0);
    }

    /**
     * This function gets called by the 'Load Game' button.
     * <p>
     * Previous saved games can be loaded for continuation. Unfortunately it is not possible to continue the log file
     * which might have been created for the saved game.
     */
    @FXML
    private void loadGame() {
        // Initialize the file chooser
        FileChooser fc = new FileChooser();
        fc.setTitle("Load Game");
        String initDir = "";
        try {
            initDir = URLDecoder.decode(this.getClass().getResource("/savegames").getPath(), "UTF-8");
        } catch(UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        fc.setInitialDirectory(new File(initDir));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Abalone Game", "*.abl"));

        // Display the file chooser
        File file = fc.showOpenDialog(this.stage);
        if(file != null) {
            String content = "";
            Game game = null;
            int mode = 0;

            // We used the ID map from the 'NetworkParser' library to store the data model.
            try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
                content = reader.readLine();
                IdMap map = GameCreator.createIdMap("42");
                game = (Game) map.decode(content);

                // If there was an AIPlayer, the regarding AI has to be set manually
                for(Player player: game.getPlayers()) {
                    if(player instanceof AIPlayer) {
                        content = reader.readLine();
                        if(content.endsWith("MiniMax")) {
                            AI<Logic, Board, Move, Player> ai = new MiniMax<>();
                            ((AIPlayer) player).setAi(ai);
                        } else if(content.endsWith("AlphaBeta")) {
                            AI<Logic, Board, Move, Player> ai = new AlphaBeta<>();
                            ((AIPlayer) player).setAi(ai);
                        } else {
                            content = reader.readLine();
                            AI<Logic, Board, Move, Player> ai = new IterativeDeepening<>();
                            ((IterativeDeepening<Logic, Board, Move, Player>) ai).setTimeLimit(Long.parseLong(content));
                            ((AIPlayer) player).setAi(ai);
                        }
                    }
                }

                // For an eventual restart we need the initial token layout for the game
                content = reader.readLine();
                mode = Integer.parseInt(content);
                reader.close();
            } catch(IOException e) {
                e.printStackTrace();
            }

            // Kill AI thread, close the file writer and re-initialize the stage with the new game
            killAIThread();
            closeWriter();
            initStage(game, mode, false);
        }
    }

    /**
     * This method gets called by the 'Save Game' menu.
     * <p>
     * Only when it's a human player's turn, it is possible to save the game for a later continuation.
     */
    @FXML
    private void saveGame() {
        // Initialize the file chooser
        FileChooser fc = new FileChooser();
        fc.setTitle("Save Game");
        String initDir = "";
        try {
            initDir = URLDecoder.decode(this.getClass().getResource("/savegames").getPath(), "UTF-8");
        } catch(UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        fc.setInitialDirectory(new File(initDir));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Abalone Game", "*.abl"));

        // Display the file chooser
        File file = fc.showSaveDialog(this.stage);
        if(file != null) {
            // We used the ID map from the 'NetworkParser' library
            IdMap map = GameCreator.createIdMap("42");
            String content = map.toJsonObject(this.game).toString();

            // Save the eventual AI types
            for(Player player: this.game.getPlayers()) {
                if(player instanceof AIPlayer) {
                    content += System.lineSeparator();
                    content += ((AIPlayer) player).getAi().getClass();
                    if(((AIPlayer) player).getAi() instanceof IterativeDeepening) {
                        content += System.lineSeparator();
                        content += ((IterativeDeepening<Logic, Board, Move, Player>) ((AIPlayer) player).getAi())
                                .getTimeLimit();
                    }
                }
            }

            // Save the initial token layout for a later restart
            content += System.lineSeparator();
            content += this.mode;

            // Write it to the file and close the writer
            try(BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
                writer.write(content);
                writer.flush();
                writer.close();
            } catch(IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This method gets called by the 'Settings' menu.
     * <p>
     * During the game it is possible to change the players' colors and/or the strengths of the AIs.
     */
    @FXML
    private void changeSettings() {
        Player player1 = this.game.getPlayers().first();
        Player player2 = this.game.getPlayers().last();

        /*
         * Show the dialog and wait for the result result = {newColor1, newStrength1, newColor2, newStrength2} or 'null'
         * If player_i is not an AI player, newStrength_i will be -1
         */
        SettingsDialog dialog = new SettingsDialog(player1, player2);
        Optional<ArrayList<String>> result = dialog.showAndWait();
        if(result.isPresent()) {
            String oldColor1 = player1.getColor();
            String oldColor2 = player2.getColor();
            String newColor1 = result.get().get(0);
            String newColor2 = result.get().get(2);

            // Both players have to play with different colors
            if(newColor1.equals(newColor2)) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Warning!");
                alert.setHeaderText("Please choose different colors for the players!");
                alert.showAndWait();
                return;
            }

            // Set new colors for the tokens and the score counter
            player1.withColor(newColor1);
            player2.withColor(newColor2);
            for(int i = 1; i <= 6; i++) {
                Circle token1 = (Circle) this.controller.getElementById("#player1_" + i);
                token1.setFill(Color.web(player2.getColor()));
                Circle token2 = (Circle) this.controller.getElementById("#player2_" + i);
                token2.setFill(Color.web(player1.getColor()));
            }
            this.boardgroup.getChildren().forEach(node -> {
                if(node instanceof Circle) {
                    Circle field = (Circle) node;
                    if(field.getFill().toString().equals(oldColor1)) {
                        field.setFill(Color.web(player1.getColor()));
                    }
                    if(field.getFill().toString().equals(oldColor2)) {
                        field.setFill(Color.web(player2.getColor()));
                    }
                }
            });

            // Set new strength for eventual AI players
            int strength1 = Integer.valueOf(result.get().get(1));
            int strength2 = Integer.valueOf(result.get().get(3));
            if(strength1 != -1) {
                if(((AIPlayer) player1).getAi() instanceof IterativeDeepening) {
                    ((IterativeDeepening<Logic, Board, Move, Player>) ((AIPlayer) player1).getAi())
                            .setTimeLimit(strength1 * 1000);
                } else {
                    ((AIPlayer) player1).withStrength(strength1);
                }

            }
            if(strength2 != -1) {
                if(((AIPlayer) player2).getAi() instanceof IterativeDeepening) {
                    ((IterativeDeepening<Logic, Board, Move, Player>) ((AIPlayer) player2).getAi())
                            .setTimeLimit(strength2 * 1000);
                } else {
                    ((AIPlayer) player2).withStrength(strength2);
                }
            }
        }
    }

    /**
     * This method gets called by the 'Rules' menu.
     * <p>
     * Displays a dialog with the game rules and how to play the game.
     */
    @FXML
    private void showRules() {
        RulesDialog rules = new RulesDialog();
        rules.showAndWait();
    }

    /**
     * This method gets called by the 'Settings' menu.
     * <p>
     * Displays a dialog with informations about the program and the developers.
     */
    @FXML
    private void showInfo() {
        InfoDialog info = new InfoDialog();
        info.showAndWait();
    }

    @Override
    public void stageClosing(WindowEvent event, Stage stage, FXStageController controller) {
        killAIThread();
        closeWriter();
        // Closing the game screen displays the start screen again.
        this.controller.showNewStage("/fxml/StartScreen.fxml", this.getClass());
    }

    @Override
    public void stageShowing(WindowEvent event, Stage stage, FXStageController controller) {
        this.stage = stage;
        this.controller = controller;
    }

    // Interrupts all AI threads
    private void killAIThread() {
        for(Player player: this.game.getPlayers()) {
            if(player instanceof AIPlayer) {
                ((AIPlayer) player).getAi().stopAiThread();
            }
        }
    }

    // Closes the logger
    private void closeWriter() {
        if(this.logger != null) {
            try {
                this.logger.close();
            } catch(IOException e) {
                e.printStackTrace();
            }
        }
    }
}