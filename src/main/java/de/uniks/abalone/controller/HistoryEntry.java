package de.uniks.abalone.controller;

import de.uniks.abalone.model.Move;

/**
 * @date 25.05.2017
 */
public class HistoryEntry {

    private String entryString;

    private Move move;

    public void setMove(Move move) {
        this.move = move;
    }

    public Move getMove() {
        return move;
    }

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public HistoryEntry(Move move, int id) {
        setMove(move);
        setId(id);
    }

    @Override
    public String toString() {
        if(entryString == null) {
            entryString = id + ". " + move;
        }
        return entryString;
    }
}
