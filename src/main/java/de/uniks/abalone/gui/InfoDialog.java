package de.uniks.abalone.gui;

import java.awt.Desktop;
import java.net.URI;

import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * The {@code InfoDialog} gives a short summary of the program and the developers.
 */
public class InfoDialog extends Dialog<Object> {
    public InfoDialog() {
        // Init stage
        Image icon = new Image(this.getClass().getResource("/gfx/info.png").toString());
        Stage stage = (Stage) this.getDialogPane().getScene().getWindow();
        stage.getIcons().add(icon);
        this.setTitle("About");
        this.getDialogPane().getButtonTypes().add(ButtonType.OK);

        // Build content
        HBox root = new HBox();
        root.setSpacing(10);

        // Logo
        ImageView image = new ImageView(new Image(this.getClass().getResource("/gfx/fachgebiet.png").toString()));

        // Description
        VBox textBox = new VBox();
        textBox.setPrefWidth(350);
        textBox.setSpacing(5);

        Label text1 = new Label("This software is the result of a bachelor project for the research"
                + " group 'Programming Languages/Methodologies' at the University of Kassel.");
        text1.setWrapText(true);
        Label text2 = new Label("The responsible professor is Prof. Dr. Claudia Fohry and our immediate"
                + " supervisor is M.Sc. Marco Bungart.");
        text2.setWrapText(true);
        Label text3 = new Label("The developers are:\n"
                + "\t- Alexander Hirsch\n"
                + "\t- Thorsten Otto\n"
                + "\t- Olaf Versteeg.\n");
        text3.setWrapText(true);
        Hyperlink link = new Hyperlink("https://www.uni-kassel.de/eecs/en/fachgebiete/plm/home.html");
        link.setOnAction(event -> {
            try {
                Desktop.getDesktop().browse(new URI("https://www.uni-kassel.de/eecs/en/fachgebiete/plm/home.html"));
            } catch(Exception e) {
                e.printStackTrace();
            }
        });

        textBox.getChildren().addAll(text1, text2, text3, link);

        root.getChildren().addAll(image, textBox);

        this.getDialogPane().setContent(root);
    }
}