package de.uniks.abalone.model.util;

import org.sdmlib.models.pattern.util.PatternObjectCreator;

import de.uniks.abalone.model.Player;
import de.uniks.networkparser.IdMap;

public class PlayerPOCreator extends PatternObjectCreator {
    @Override
    public Object getSendableInstance(boolean reference) {
        if(reference) {
            return new PlayerPO(new Player[] {});
        } else {
            return new PlayerPO();
        }
    }

    public static IdMap createIdMap(String sessionID) {
        return de.uniks.abalone.model.util.CreatorCreator.createIdMap(sessionID);
    }
}
