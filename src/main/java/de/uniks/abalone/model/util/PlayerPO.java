package de.uniks.abalone.model.util;

import org.sdmlib.models.pattern.AttributeConstraint;
import org.sdmlib.models.pattern.Pattern;
import org.sdmlib.models.pattern.PatternObject;

import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.model.Token;

public class PlayerPO extends PatternObject<PlayerPO, Player> {
    public PlayerSet allMatches() {
        setDoAllMatches(true);
        PlayerSet matches = new PlayerSet();
        while(getPattern().getHasMatch()) {
            matches.add(getCurrentMatch());
            getPattern().findMatch();
        }
        return matches;
    }

    public PlayerPO() {
        newInstance(null);
    }

    public PlayerPO(Player... hostGraphObject) {
        if(hostGraphObject == null || hostGraphObject.length < 1) {
            return;
        }
        newInstance(null, hostGraphObject);
    }

    public PlayerPO(String modifier) {
        setModifier(modifier);
    }

    public PlayerPO createNameCondition(String value) {
        new AttributeConstraint()
                .withAttrName(Player.PROPERTY_NAME)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(getPattern().getModifier())
                .withPattern(getPattern());
        super.filterAttr();
        return this;
    }

    public PlayerPO createNameCondition(String lower, String upper) {
        new AttributeConstraint()
                .withAttrName(Player.PROPERTY_NAME)
                .withTgtValue(lower)
                .withUpperTgtValue(upper)
                .withSrc(this)
                .withModifier(getPattern().getModifier())
                .withPattern(getPattern());
        super.filterAttr();
        return this;
    }

    public PlayerPO createNameAssignment(String value) {
        new AttributeConstraint()
                .withAttrName(Player.PROPERTY_NAME)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(Pattern.CREATE)
                .withPattern(getPattern());
        super.filterAttr();
        return this;
    }

    public String getName() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getName();
        }
        return null;
    }

    public PlayerPO withName(String value) {
        if(getPattern().getHasMatch()) {
            getCurrentMatch().setName(value);
        }
        return this;
    }

    public PlayerPO createColorCondition(String value) {
        new AttributeConstraint()
                .withAttrName(Player.PROPERTY_COLOR)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(getPattern().getModifier())
                .withPattern(getPattern());
        super.filterAttr();
        return this;
    }

    public PlayerPO createColorCondition(String lower, String upper) {
        new AttributeConstraint()
                .withAttrName(Player.PROPERTY_COLOR)
                .withTgtValue(lower)
                .withUpperTgtValue(upper)
                .withSrc(this)
                .withModifier(getPattern().getModifier())
                .withPattern(getPattern());
        super.filterAttr();
        return this;
    }

    public PlayerPO createColorAssignment(String value) {
        new AttributeConstraint()
                .withAttrName(Player.PROPERTY_COLOR)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(Pattern.CREATE)
                .withPattern(getPattern());
        super.filterAttr();
        return this;
    }

    public String getColor() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getColor();
        }
        return null;
    }

    public PlayerPO withColor(String value) {
        if(getPattern().getHasMatch()) {
            getCurrentMatch().setColor(value);
        }
        return this;
    }

    public PlayerPO createScoresCondition(int value) {
        new AttributeConstraint()
                .withAttrName(Player.PROPERTY_SCORES)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(getPattern().getModifier())
                .withPattern(getPattern());
        super.filterAttr();
        return this;
    }

    public PlayerPO createScoresCondition(int lower, int upper) {
        new AttributeConstraint()
                .withAttrName(Player.PROPERTY_SCORES)
                .withTgtValue(lower)
                .withUpperTgtValue(upper)
                .withSrc(this)
                .withModifier(getPattern().getModifier())
                .withPattern(getPattern());
        super.filterAttr();
        return this;
    }

    public PlayerPO createScoresAssignment(int value) {
        new AttributeConstraint()
                .withAttrName(Player.PROPERTY_SCORES)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(Pattern.CREATE)
                .withPattern(getPattern());
        super.filterAttr();
        return this;
    }

    public int getScores() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getScores();
        }
        return 0;
    }

    public PlayerPO withScores(int value) {
        if(getPattern().getHasMatch()) {
            getCurrentMatch().setScores(value);
        }
        return this;
    }

    public GamePO createGamePO() {
        GamePO result = new GamePO(new Game[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Player.PROPERTY_GAME, result);
        return result;
    }

    public GamePO createGamePO(String modifier) {
        GamePO result = new GamePO(new Game[] {});
        result.setModifier(modifier);
        super.hasLink(Player.PROPERTY_GAME, result);
        return result;
    }

    public PlayerPO createGameLink(GamePO tgt) {
        return hasLinkConstraint(tgt, Player.PROPERTY_GAME);
    }

    public PlayerPO createGameLink(GamePO tgt, String modifier) {
        return hasLinkConstraint(tgt, Player.PROPERTY_GAME, modifier);
    }

    public Game getGame() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getGame();
        }
        return null;
    }

    public GamePO createCurrentGamePO() {
        GamePO result = new GamePO(new Game[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Player.PROPERTY_CURRENTGAME, result);
        return result;
    }

    public GamePO createCurrentGamePO(String modifier) {
        GamePO result = new GamePO(new Game[] {});
        result.setModifier(modifier);
        super.hasLink(Player.PROPERTY_CURRENTGAME, result);
        return result;
    }

    public PlayerPO createCurrentGameLink(GamePO tgt) {
        return hasLinkConstraint(tgt, Player.PROPERTY_CURRENTGAME);
    }

    public PlayerPO createCurrentGameLink(GamePO tgt, String modifier) {
        return hasLinkConstraint(tgt, Player.PROPERTY_CURRENTGAME, modifier);
    }

    public Game getCurrentGame() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getCurrentGame();
        }
        return null;
    }

    public GamePO createWonGamePO() {
        GamePO result = new GamePO(new Game[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Player.PROPERTY_WONGAME, result);
        return result;
    }

    public GamePO createWonGamePO(String modifier) {
        GamePO result = new GamePO(new Game[] {});
        result.setModifier(modifier);
        super.hasLink(Player.PROPERTY_WONGAME, result);
        return result;
    }

    public PlayerPO createWonGameLink(GamePO tgt) {
        return hasLinkConstraint(tgt, Player.PROPERTY_WONGAME);
    }

    public PlayerPO createWonGameLink(GamePO tgt, String modifier) {
        return hasLinkConstraint(tgt, Player.PROPERTY_WONGAME, modifier);
    }

    public Game getWonGame() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getWonGame();
        }
        return null;
    }

    public TokenPO createTokensPO() {
        TokenPO result = new TokenPO(new Token[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Player.PROPERTY_TOKENS, result);
        return result;
    }

    public TokenPO createTokensPO(String modifier) {
        TokenPO result = new TokenPO(new Token[] {});
        result.setModifier(modifier);
        super.hasLink(Player.PROPERTY_TOKENS, result);
        return result;
    }

    public PlayerPO createTokensLink(TokenPO tgt) {
        return hasLinkConstraint(tgt, Player.PROPERTY_TOKENS);
    }

    public PlayerPO createTokensLink(TokenPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Player.PROPERTY_TOKENS, modifier);
    }

    public TokenSet getTokens() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getTokens();
        }
        return null;
    }

    public PlayerPO createPreviousPO() {
        PlayerPO result = new PlayerPO(new Player[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Player.PROPERTY_PREVIOUS, result);
        return result;
    }

    public PlayerPO createPreviousPO(String modifier) {
        PlayerPO result = new PlayerPO(new Player[] {});
        result.setModifier(modifier);
        super.hasLink(Player.PROPERTY_PREVIOUS, result);
        return result;
    }

    public PlayerPO createPreviousLink(PlayerPO tgt) {
        return hasLinkConstraint(tgt, Player.PROPERTY_PREVIOUS);
    }

    public PlayerPO createPreviousLink(PlayerPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Player.PROPERTY_PREVIOUS, modifier);
    }

    public Player getPrevious() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getPrevious();
        }
        return null;
    }

    public PlayerPO createNextPO() {
        PlayerPO result = new PlayerPO(new Player[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Player.PROPERTY_NEXT, result);
        return result;
    }

    public PlayerPO createNextPO(String modifier) {
        PlayerPO result = new PlayerPO(new Player[] {});
        result.setModifier(modifier);
        super.hasLink(Player.PROPERTY_NEXT, result);
        return result;
    }

    public PlayerPO createNextLink(PlayerPO tgt) {
        return hasLinkConstraint(tgt, Player.PROPERTY_NEXT);
    }

    public PlayerPO createNextLink(PlayerPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Player.PROPERTY_NEXT, modifier);
    }

    public Player getNext() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getNext();
        }
        return null;
    }
}
