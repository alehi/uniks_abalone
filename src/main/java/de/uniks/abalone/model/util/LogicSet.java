/*
 * Copyright (c) 2017 Olaf Versteeg Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.uniks.abalone.model.util;

import java.util.Collection;

import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Logic;
import de.uniks.abalone.model.Move;
import de.uniks.networkparser.interfaces.Condition;
import de.uniks.networkparser.list.ObjectSet;
import de.uniks.networkparser.list.SimpleSet;
import de.uniks.abalone.model.util.GameSet;
import de.uniks.abalone.model.util.MoveSet;

public class LogicSet extends SimpleSet<Logic> {
    @Override
    protected Class<?> getTypClass() {
        return Logic.class;
    }

    public LogicSet() {
        // empty
    }

    public LogicSet(Logic... objects) {
        for(Logic obj: objects) {
            this.add(obj);
        }
    }

    public LogicSet(Collection<Logic> objects) {
        this.addAll(objects);
    }

    public static final LogicSet EMPTY_SET = new LogicSet().withFlag(LogicSet.READONLY);

    public LogicPO createLogicPO() {
        return new LogicPO(this.toArray(new Logic[size()]));
    }

    public String getEntryType() {
        return "de.uniks.abalone.model.Logic";
    }

    @Override
    public LogicSet getNewList(boolean keyValue) {
        return new LogicSet();
    }

    @Override
    public LogicSet filter(Condition<Logic> condition) {
        LogicSet filterList = new LogicSet();
        filterItems(filterList, condition);
        return filterList;
    }

    @SuppressWarnings("unchecked")
    public LogicSet with(Object value) {
        if(value == null) {
            return this;
        } else if(value instanceof java.util.Collection) {
            this.addAll((Collection<Logic>) value);
        } else if(value != null) {
            this.add((Logic) value);
        }
        return this;
    }

    public LogicSet without(Logic value) {
        this.remove(value);
        return this;
    }

    /**
     * Loop through the current set of Logic objects and collect a set of the Game objects reached via game.
     *
     * @return Set of Game objects reachable via game
     */
    public GameSet getGame() {
        GameSet result = new GameSet();
        for(Logic obj: this) {
            result.with(obj.getGame());
        }
        return result;
    }

    /**
     * Loop through the current set of Logic objects and collect all contained objects with reference game pointing to
     * the object passed as parameter.
     *
     * @param value The object required as game neighbor of the collected results.
     *
     * @return Set of Game objects referring to value via game
     */
    public LogicSet filterGame(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        LogicSet answer = new LogicSet();
        for(Logic obj: this) {
            if(neighbors.contains(obj.getGame()) || (neighbors.isEmpty() && obj.getGame() == null)) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Logic object passed as parameter to the Game
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their Game attributes.
     */
    public LogicSet withGame(Game value) {
        for(Logic obj: this) {
            obj.withGame(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Logic objects and collect a set of the Move objects reached via currentMove.
     * 
     * @return Set of Move objects reachable via currentMove
     */
    public MoveSet getCurrentMove() {
        MoveSet result = new MoveSet();

        for(Logic obj: this) {
            result.with(obj.getCurrentMove());
        }

        return result;
    }

    /**
     * Loop through the current set of Logic objects and collect all contained objects with reference currentMove
     * pointing to the object passed as parameter.
     * 
     * @param value The object required as currentMove neighbor of the collected results.
     * 
     * @return Set of Move objects referring to value via currentMove
     */
    public LogicSet filterCurrentMove(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        LogicSet answer = new LogicSet();

        for(Logic obj: this) {
            if(neighbors.contains(obj.getCurrentMove()) || (neighbors.isEmpty() && obj.getCurrentMove() == null)) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Logic object passed as parameter to the CurrentMove
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their CurrentMove attributes.
     */
    public LogicSet withCurrentMove(Move value) {
        for(Logic obj: this) {
            obj.withCurrentMove(value);
        }

        return this;
    }

}
