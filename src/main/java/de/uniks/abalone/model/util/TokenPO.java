package de.uniks.abalone.model.util;

import org.sdmlib.models.pattern.PatternObject;

import de.uniks.abalone.model.Field;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.model.Token;

public class TokenPO extends PatternObject<TokenPO, Token> {
    public TokenSet allMatches() {
        setDoAllMatches(true);
        TokenSet matches = new TokenSet();
        while(getPattern().getHasMatch()) {
            matches.add(getCurrentMatch());
            getPattern().findMatch();
        }
        return matches;
    }

    public TokenPO() {
        newInstance(null);
    }

    public TokenPO(Token... hostGraphObject) {
        if(hostGraphObject == null || hostGraphObject.length < 1) {
            return;
        }
        newInstance(null, hostGraphObject);
    }

    public TokenPO(String modifier) {
        setModifier(modifier);
    }

    public GamePO createGamePO() {
        GamePO result = new GamePO(new Game[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Token.PROPERTY_GAME, result);
        return result;
    }

    public GamePO createGamePO(String modifier) {
        GamePO result = new GamePO(new Game[] {});
        result.setModifier(modifier);
        super.hasLink(Token.PROPERTY_GAME, result);
        return result;
    }

    public TokenPO createGameLink(GamePO tgt) {
        return hasLinkConstraint(tgt, Token.PROPERTY_GAME);
    }

    public TokenPO createGameLink(GamePO tgt, String modifier) {
        return hasLinkConstraint(tgt, Token.PROPERTY_GAME, modifier);
    }

    public Game getGame() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getGame();
        }
        return null;
    }

    public FieldPO createFieldPO() {
        FieldPO result = new FieldPO(new Field[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Token.PROPERTY_FIELD, result);
        return result;
    }

    public FieldPO createFieldPO(String modifier) {
        FieldPO result = new FieldPO(new Field[] {});
        result.setModifier(modifier);
        super.hasLink(Token.PROPERTY_FIELD, result);
        return result;
    }

    public TokenPO createFieldLink(FieldPO tgt) {
        return hasLinkConstraint(tgt, Token.PROPERTY_FIELD);
    }

    public TokenPO createFieldLink(FieldPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Token.PROPERTY_FIELD, modifier);
    }

    public Field getField() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getField();
        }
        return null;
    }

    public PlayerPO createPlayerPO() {
        PlayerPO result = new PlayerPO(new Player[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Token.PROPERTY_PLAYER, result);
        return result;
    }

    public PlayerPO createPlayerPO(String modifier) {
        PlayerPO result = new PlayerPO(new Player[] {});
        result.setModifier(modifier);
        super.hasLink(Token.PROPERTY_PLAYER, result);
        return result;
    }

    public TokenPO createPlayerLink(PlayerPO tgt) {
        return hasLinkConstraint(tgt, Token.PROPERTY_PLAYER);
    }

    public TokenPO createPlayerLink(PlayerPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Token.PROPERTY_PLAYER, modifier);
    }

    public Player getPlayer() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getPlayer();
        }
        return null;
    }
}
