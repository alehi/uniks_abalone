package de.uniks.abalone.model.util;

import org.sdmlib.models.pattern.AttributeConstraint;
import org.sdmlib.models.pattern.Pattern;
import org.sdmlib.models.pattern.PatternObject;

import de.uniks.abalone.model.Board;
import de.uniks.abalone.model.Field;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Token;

public class FieldPO extends PatternObject<FieldPO, Field> {
    public FieldSet allMatches() {
        setDoAllMatches(true);
        FieldSet matches = new FieldSet();
        while(getPattern().getHasMatch()) {
            matches.add(getCurrentMatch());
            getPattern().findMatch();
        }
        return matches;
    }

    public FieldPO() {
        newInstance(null);
    }

    public FieldPO(Field... hostGraphObject) {
        if(hostGraphObject == null || hostGraphObject.length < 1) {
            return;
        }
        newInstance(null, hostGraphObject);
    }

    public FieldPO(String modifier) {
        setModifier(modifier);
    }

    public FieldPO createXCondition(int value) {
        new AttributeConstraint()
                .withAttrName(Field.PROPERTY_X)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(getPattern().getModifier())
                .withPattern(getPattern());
        super.filterAttr();
        return this;
    }

    public FieldPO createXCondition(int lower, int upper) {
        new AttributeConstraint()
                .withAttrName(Field.PROPERTY_X)
                .withTgtValue(lower)
                .withUpperTgtValue(upper)
                .withSrc(this)
                .withModifier(getPattern().getModifier())
                .withPattern(getPattern());
        super.filterAttr();
        return this;
    }

    public FieldPO createXAssignment(int value) {
        new AttributeConstraint()
                .withAttrName(Field.PROPERTY_X)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(Pattern.CREATE)
                .withPattern(getPattern());
        super.filterAttr();
        return this;
    }

    public int getX() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getX();
        }
        return 0;
    }

    public FieldPO withX(int value) {
        if(getPattern().getHasMatch()) {
            getCurrentMatch().setX(value);
        }
        return this;
    }

    public FieldPO createYCondition(int value) {
        new AttributeConstraint()
                .withAttrName(Field.PROPERTY_Y)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(getPattern().getModifier())
                .withPattern(getPattern());
        super.filterAttr();
        return this;
    }

    public FieldPO createYCondition(int lower, int upper) {
        new AttributeConstraint()
                .withAttrName(Field.PROPERTY_Y)
                .withTgtValue(lower)
                .withUpperTgtValue(upper)
                .withSrc(this)
                .withModifier(getPattern().getModifier())
                .withPattern(getPattern());
        super.filterAttr();
        return this;
    }

    public FieldPO createYAssignment(int value) {
        new AttributeConstraint()
                .withAttrName(Field.PROPERTY_Y)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(Pattern.CREATE)
                .withPattern(getPattern());
        super.filterAttr();
        return this;
    }

    public int getY() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getY();
        }
        return 0;
    }

    public FieldPO withY(int value) {
        if(getPattern().getHasMatch()) {
            getCurrentMatch().setY(value);
        }
        return this;
    }

    public GamePO createGamePO() {
        GamePO result = new GamePO(new Game[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Field.PROPERTY_GAME, result);
        return result;
    }

    public GamePO createGamePO(String modifier) {
        GamePO result = new GamePO(new Game[] {});
        result.setModifier(modifier);
        super.hasLink(Field.PROPERTY_GAME, result);
        return result;
    }

    public FieldPO createGameLink(GamePO tgt) {
        return hasLinkConstraint(tgt, Field.PROPERTY_GAME);
    }

    public FieldPO createGameLink(GamePO tgt, String modifier) {
        return hasLinkConstraint(tgt, Field.PROPERTY_GAME, modifier);
    }

    public Game getGame() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getGame();
        }
        return null;
    }

    public BoardPO createBoardPO() {
        BoardPO result = new BoardPO(new Board[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Field.PROPERTY_BOARD, result);
        return result;
    }

    public BoardPO createBoardPO(String modifier) {
        BoardPO result = new BoardPO(new Board[] {});
        result.setModifier(modifier);
        super.hasLink(Field.PROPERTY_BOARD, result);
        return result;
    }

    public FieldPO createBoardLink(BoardPO tgt) {
        return hasLinkConstraint(tgt, Field.PROPERTY_BOARD);
    }

    public FieldPO createBoardLink(BoardPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Field.PROPERTY_BOARD, modifier);
    }

    public Board getBoard() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getBoard();
        }
        return null;
    }

    public TokenPO createTokenPO() {
        TokenPO result = new TokenPO(new Token[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Field.PROPERTY_TOKEN, result);
        return result;
    }

    public TokenPO createTokenPO(String modifier) {
        TokenPO result = new TokenPO(new Token[] {});
        result.setModifier(modifier);
        super.hasLink(Field.PROPERTY_TOKEN, result);
        return result;
    }

    public FieldPO createTokenLink(TokenPO tgt) {
        return hasLinkConstraint(tgt, Field.PROPERTY_TOKEN);
    }

    public FieldPO createTokenLink(TokenPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Field.PROPERTY_TOKEN, modifier);
    }

    public Token getToken() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getToken();
        }
        return null;
    }

    public FieldPO createRightPO() {
        FieldPO result = new FieldPO(new Field[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Field.PROPERTY_RIGHT, result);
        return result;
    }

    public FieldPO createRightPO(String modifier) {
        FieldPO result = new FieldPO(new Field[] {});
        result.setModifier(modifier);
        super.hasLink(Field.PROPERTY_RIGHT, result);
        return result;
    }

    public FieldPO createRightLink(FieldPO tgt) {
        return hasLinkConstraint(tgt, Field.PROPERTY_RIGHT);
    }

    public FieldPO createRightLink(FieldPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Field.PROPERTY_RIGHT, modifier);
    }

    public Field getRight() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getRight();
        }
        return null;
    }

    public FieldPO createLeftPO() {
        FieldPO result = new FieldPO(new Field[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Field.PROPERTY_LEFT, result);
        return result;
    }

    public FieldPO createLeftPO(String modifier) {
        FieldPO result = new FieldPO(new Field[] {});
        result.setModifier(modifier);
        super.hasLink(Field.PROPERTY_LEFT, result);
        return result;
    }

    public FieldPO createLeftLink(FieldPO tgt) {
        return hasLinkConstraint(tgt, Field.PROPERTY_LEFT);
    }

    public FieldPO createLeftLink(FieldPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Field.PROPERTY_LEFT, modifier);
    }

    public Field getLeft() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getLeft();
        }
        return null;
    }

    public FieldPO createDownLeftPO() {
        FieldPO result = new FieldPO(new Field[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Field.PROPERTY_DOWNLEFT, result);
        return result;
    }

    public FieldPO createDownLeftPO(String modifier) {
        FieldPO result = new FieldPO(new Field[] {});
        result.setModifier(modifier);
        super.hasLink(Field.PROPERTY_DOWNLEFT, result);
        return result;
    }

    public FieldPO createDownLeftLink(FieldPO tgt) {
        return hasLinkConstraint(tgt, Field.PROPERTY_DOWNLEFT);
    }

    public FieldPO createDownLeftLink(FieldPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Field.PROPERTY_DOWNLEFT, modifier);
    }

    public Field getDownLeft() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getDownLeft();
        }
        return null;
    }

    public FieldPO createTopRightPO() {
        FieldPO result = new FieldPO(new Field[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Field.PROPERTY_TOPRIGHT, result);
        return result;
    }

    public FieldPO createTopRightPO(String modifier) {
        FieldPO result = new FieldPO(new Field[] {});
        result.setModifier(modifier);
        super.hasLink(Field.PROPERTY_TOPRIGHT, result);
        return result;
    }

    public FieldPO createTopRightLink(FieldPO tgt) {
        return hasLinkConstraint(tgt, Field.PROPERTY_TOPRIGHT);
    }

    public FieldPO createTopRightLink(FieldPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Field.PROPERTY_TOPRIGHT, modifier);
    }

    public Field getTopRight() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getTopRight();
        }
        return null;
    }

    public FieldPO createDownRightPO() {
        FieldPO result = new FieldPO(new Field[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Field.PROPERTY_DOWNRIGHT, result);
        return result;
    }

    public FieldPO createDownRightPO(String modifier) {
        FieldPO result = new FieldPO(new Field[] {});
        result.setModifier(modifier);
        super.hasLink(Field.PROPERTY_DOWNRIGHT, result);
        return result;
    }

    public FieldPO createDownRightLink(FieldPO tgt) {
        return hasLinkConstraint(tgt, Field.PROPERTY_DOWNRIGHT);
    }

    public FieldPO createDownRightLink(FieldPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Field.PROPERTY_DOWNRIGHT, modifier);
    }

    public Field getDownRight() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getDownRight();
        }
        return null;
    }

    public FieldPO createTopLeftPO() {
        FieldPO result = new FieldPO(new Field[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Field.PROPERTY_TOPLEFT, result);
        return result;
    }

    public FieldPO createTopLeftPO(String modifier) {
        FieldPO result = new FieldPO(new Field[] {});
        result.setModifier(modifier);
        super.hasLink(Field.PROPERTY_TOPLEFT, result);
        return result;
    }

    public FieldPO createTopLeftLink(FieldPO tgt) {
        return hasLinkConstraint(tgt, Field.PROPERTY_TOPLEFT);
    }

    public FieldPO createTopLeftLink(FieldPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Field.PROPERTY_TOPLEFT, modifier);
    }

    public Field getTopLeft() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getTopLeft();
        }
        return null;
    }

    public FieldPO createSelectedCondition(boolean value) {
        new AttributeConstraint()
                .withAttrName(Field.PROPERTY_SELECTED)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public FieldPO createSelectedAssignment(boolean value) {
        new AttributeConstraint()
                .withAttrName(Field.PROPERTY_SELECTED)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(Pattern.CREATE)
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public boolean getSelected() {
        if(this.getPattern().getHasMatch()) {
            return ((Field) getCurrentMatch()).isSelected();
        }
        return false;
    }

    public FieldPO withSelected(boolean value) {
        if(this.getPattern().getHasMatch()) {
            ((Field) getCurrentMatch()).setSelected(value);
        }
        return this;
    }

    public FieldPO createLastMovedCondition(boolean value) {
        new AttributeConstraint()
                .withAttrName(Field.PROPERTY_LASTMOVED)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public FieldPO createLastMovedAssignment(boolean value) {
        new AttributeConstraint()
                .withAttrName(Field.PROPERTY_LASTMOVED)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(Pattern.CREATE)
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public boolean getLastMoved() {
        if(this.getPattern().getHasMatch()) {
            return ((Field) getCurrentMatch()).isLastMoved();
        }
        return false;
    }

    public FieldPO withLastMoved(boolean value) {
        if(this.getPattern().getHasMatch()) {
            ((Field) getCurrentMatch()).setLastMoved(value);
        }
        return this;
    }

}
