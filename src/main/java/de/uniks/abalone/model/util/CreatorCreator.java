package de.uniks.abalone.model.util;

import de.uniks.networkparser.IdMap;

class CreatorCreator {
    public static IdMap createIdMap(String sessionID) {
        IdMap jsonIdMap = new IdMap().withSessionId(sessionID);
        jsonIdMap.with(new GameCreator());
        jsonIdMap.with(new GamePOCreator());
        jsonIdMap.with(new BoardCreator());
        jsonIdMap.with(new BoardPOCreator());
        jsonIdMap.with(new FieldCreator());
        jsonIdMap.with(new FieldPOCreator());
        jsonIdMap.with(new TokenCreator());
        jsonIdMap.with(new TokenPOCreator());
        jsonIdMap.with(new PlayerCreator());
        jsonIdMap.with(new PlayerPOCreator());
        jsonIdMap.with(new MoveCreator());
        jsonIdMap.with(new MovePOCreator());
        jsonIdMap.with(new LogicCreator());
        jsonIdMap.with(new LogicPOCreator());
        jsonIdMap.with(new GameCreator());
        jsonIdMap.with(new GamePOCreator());
        jsonIdMap.with(new BoardCreator());
        jsonIdMap.with(new BoardPOCreator());
        jsonIdMap.with(new FieldCreator());
        jsonIdMap.with(new FieldPOCreator());
        jsonIdMap.with(new TokenCreator());
        jsonIdMap.with(new TokenPOCreator());
        jsonIdMap.with(new PlayerCreator());
        jsonIdMap.with(new PlayerPOCreator());
        jsonIdMap.with(new MoveCreator());
        jsonIdMap.with(new MovePOCreator());
        jsonIdMap.with(new AIPlayerCreator());
        jsonIdMap.with(new AIPlayerPOCreator());
        jsonIdMap.with(new LogicCreator());
        jsonIdMap.with(new LogicPOCreator());
        jsonIdMap.with(new GameCreator());
        jsonIdMap.with(new GamePOCreator());
        jsonIdMap.with(new BoardCreator());
        jsonIdMap.with(new BoardPOCreator());
        jsonIdMap.with(new FieldCreator());
        jsonIdMap.with(new FieldPOCreator());
        jsonIdMap.with(new TokenCreator());
        jsonIdMap.with(new TokenPOCreator());
        jsonIdMap.with(new PlayerCreator());
        jsonIdMap.with(new PlayerPOCreator());
        jsonIdMap.with(new MoveCreator());
        jsonIdMap.with(new MovePOCreator());
        jsonIdMap.with(new AIPlayerCreator());
        jsonIdMap.with(new AIPlayerPOCreator());
        jsonIdMap.with(new LogicCreator());
        jsonIdMap.with(new LogicPOCreator());

        jsonIdMap.with(new GameCreator());
        jsonIdMap.with(new GamePOCreator());
        jsonIdMap.with(new BoardCreator());
        jsonIdMap.with(new BoardPOCreator());
        jsonIdMap.with(new FieldCreator());
        jsonIdMap.with(new FieldPOCreator());
        jsonIdMap.with(new TokenCreator());
        jsonIdMap.with(new TokenPOCreator());
        jsonIdMap.with(new PlayerCreator());
        jsonIdMap.with(new PlayerPOCreator());
        jsonIdMap.with(new MoveCreator());
        jsonIdMap.with(new MovePOCreator());
        jsonIdMap.with(new AIPlayerCreator());
        jsonIdMap.with(new AIPlayerPOCreator());
        jsonIdMap.with(new LogicCreator());
        jsonIdMap.with(new LogicPOCreator());
        return jsonIdMap;
    }
}
