/*
 * Copyright (c) 2017 Olaf Versteeg Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.uniks.abalone.model.util;

import java.util.Collection;

import de.uniks.abalone.model.Field;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.model.Token;
import de.uniks.networkparser.interfaces.Condition;
import de.uniks.networkparser.list.ObjectSet;
import de.uniks.networkparser.list.SimpleSet;
import de.uniks.abalone.model.util.GameSet;
import de.uniks.abalone.model.util.FieldSet;
import de.uniks.abalone.model.util.PlayerSet;

public class TokenSet extends SimpleSet<Token> {
    @Override
    protected Class<?> getTypClass() {
        return Token.class;
    }

    public TokenSet() {
        // empty
    }

    public TokenSet(Token... objects) {
        for(Token obj: objects) {
            this.add(obj);
        }
    }

    public TokenSet(Collection<Token> objects) {
        this.addAll(objects);
    }

    public static final TokenSet EMPTY_SET = new TokenSet().withFlag(TokenSet.READONLY);

    public TokenPO createTokenPO() {
        return new TokenPO(this.toArray(new Token[size()]));
    }

    public String getEntryType() {
        return "de.uniks.abalone.model.Token";
    }

    @Override
    public TokenSet getNewList(boolean keyValue) {
        return new TokenSet();
    }

    @Override
    public TokenSet filter(Condition<Token> condition) {
        TokenSet filterList = new TokenSet();
        filterItems(filterList, condition);
        return filterList;
    }

    @SuppressWarnings("unchecked")
    public TokenSet with(Object value) {
        if(value == null) {
            return this;
        } else if(value instanceof java.util.Collection) {
            this.addAll((Collection<Token>) value);
        } else if(value != null) {
            this.add((Token) value);
        }
        return this;
    }

    public TokenSet without(Token value) {
        this.remove(value);
        return this;
    }

    /**
     * Loop through the current set of Token objects and collect a set of the Game objects reached via game.
     *
     * @return Set of Game objects reachable via game
     */
    public GameSet getGame() {
        GameSet result = new GameSet();
        for(Token obj: this) {
            result.with(obj.getGame());
        }
        return result;
    }

    /**
     * Loop through the current set of Token objects and collect all contained objects with reference game pointing to
     * the object passed as parameter.
     *
     * @param value The object required as game neighbor of the collected results.
     *
     * @return Set of Game objects referring to value via game
     */
    public TokenSet filterGame(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        TokenSet answer = new TokenSet();
        for(Token obj: this) {
            if(neighbors.contains(obj.getGame()) || (neighbors.isEmpty() && obj.getGame() == null)) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Token object passed as parameter to the Game
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their Game attributes.
     */
    public TokenSet withGame(Game value) {
        for(Token obj: this) {
            obj.withGame(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Token objects and collect a set of the Field objects reached via field.
     *
     * @return Set of Field objects reachable via field
     */
    public FieldSet getField() {
        FieldSet result = new FieldSet();
        for(Token obj: this) {
            result.with(obj.getField());
        }
        return result;
    }

    /**
     * Loop through the current set of Token objects and collect all contained objects with reference field pointing to
     * the object passed as parameter.
     *
     * @param value The object required as field neighbor of the collected results.
     *
     * @return Set of Field objects referring to value via field
     */
    public TokenSet filterField(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        TokenSet answer = new TokenSet();
        for(Token obj: this) {
            if(neighbors.contains(obj.getField()) || (neighbors.isEmpty() && obj.getField() == null)) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Token object passed as parameter to the Field
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their Field attributes.
     */
    public TokenSet withField(Field value) {
        for(Token obj: this) {
            obj.withField(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Token objects and collect a set of the Player objects reached via player.
     *
     * @return Set of Player objects reachable via player
     */
    public PlayerSet getPlayer() {
        PlayerSet result = new PlayerSet();
        for(Token obj: this) {
            result.with(obj.getPlayer());
        }
        return result;
    }

    /**
     * Loop through the current set of Token objects and collect all contained objects with reference player pointing to
     * the object passed as parameter.
     *
     * @param value The object required as player neighbor of the collected results.
     *
     * @return Set of Player objects referring to value via player
     */
    public TokenSet filterPlayer(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        TokenSet answer = new TokenSet();
        for(Token obj: this) {
            if(neighbors.contains(obj.getPlayer()) || (neighbors.isEmpty() && obj.getPlayer() == null)) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Token object passed as parameter to the Player
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their Player attributes.
     */
    public TokenSet withPlayer(Player value) {
        for(Token obj: this) {
            obj.withPlayer(value);
        }
        return this;
    }
}
