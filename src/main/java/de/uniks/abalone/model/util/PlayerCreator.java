/*
 * Copyright (c) 2017 Olaf Versteeg Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.uniks.abalone.model.util;

import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.model.Token;
import de.uniks.networkparser.IdMap;
import de.uniks.networkparser.interfaces.SendableEntityCreator;

public class PlayerCreator implements SendableEntityCreator {
    private final String[] properties = new String[] {
            Player.PROPERTY_NAME,
            Player.PROPERTY_COLOR,
            Player.PROPERTY_SCORES,
            Player.PROPERTY_GAME,
            Player.PROPERTY_CURRENTGAME,
            Player.PROPERTY_WONGAME,
            Player.PROPERTY_TOKENS,
            Player.PROPERTY_PREVIOUS,
            Player.PROPERTY_NEXT,
    };

    @Override
    public String[] getProperties() {
        return properties;
    }

    @Override
    public Object getSendableInstance(boolean reference) {
        return new Player();
    }

    @Override
    public Object getValue(Object target, String attrName) {
        int pos = attrName.indexOf('.');
        String attribute = attrName;
        if(pos > 0) {
            attribute = attrName.substring(0, pos);
        }
        if(Player.PROPERTY_NAME.equalsIgnoreCase(attribute)) {
            return ((Player) target).getName();
        }
        if(Player.PROPERTY_COLOR.equalsIgnoreCase(attribute)) {
            return ((Player) target).getColor();
        }
        if(Player.PROPERTY_SCORES.equalsIgnoreCase(attribute)) {
            return ((Player) target).getScores();
        }
        if(Player.PROPERTY_GAME.equalsIgnoreCase(attribute)) {
            return ((Player) target).getGame();
        }
        if(Player.PROPERTY_CURRENTGAME.equalsIgnoreCase(attribute)) {
            return ((Player) target).getCurrentGame();
        }
        if(Player.PROPERTY_WONGAME.equalsIgnoreCase(attribute)) {
            return ((Player) target).getWonGame();
        }
        if(Player.PROPERTY_TOKENS.equalsIgnoreCase(attribute)) {
            return ((Player) target).getTokens();
        }
        if(Player.PROPERTY_PREVIOUS.equalsIgnoreCase(attribute)) {
            return ((Player) target).getPrevious();
        }
        if(Player.PROPERTY_NEXT.equalsIgnoreCase(attribute)) {
            return ((Player) target).getNext();
        }
        return null;
    }

    @Override
    public boolean setValue(Object target, String attrName, Object value, String type) {
        if(Player.PROPERTY_SCORES.equalsIgnoreCase(attrName)) {
            ((Player) target).setScores(Integer.parseInt(value.toString()));
            return true;
        }
        if(Player.PROPERTY_COLOR.equalsIgnoreCase(attrName)) {
            ((Player) target).setColor((String) value);
            return true;
        }
        if(Player.PROPERTY_NAME.equalsIgnoreCase(attrName)) {
            ((Player) target).setName((String) value);
            return true;
        }
        if(SendableEntityCreator.REMOVE.equals(type) && value != null) {
            attrName = attrName + type;
        }
        if(Player.PROPERTY_GAME.equalsIgnoreCase(attrName)) {
            ((Player) target).setGame((Game) value);
            return true;
        }
        if(Player.PROPERTY_CURRENTGAME.equalsIgnoreCase(attrName)) {
            ((Player) target).setCurrentGame((Game) value);
            return true;
        }
        if(Player.PROPERTY_WONGAME.equalsIgnoreCase(attrName)) {
            ((Player) target).setWonGame((Game) value);
            return true;
        }
        if(Player.PROPERTY_TOKENS.equalsIgnoreCase(attrName)) {
            ((Player) target).withTokens((Token) value);
            return true;
        }
        if((Player.PROPERTY_TOKENS + SendableEntityCreator.REMOVE).equalsIgnoreCase(attrName)) {
            ((Player) target).withoutTokens((Token) value);
            return true;
        }
        if(Player.PROPERTY_PREVIOUS.equalsIgnoreCase(attrName)) {
            ((Player) target).setPrevious((Player) value);
            return true;
        }
        if(Player.PROPERTY_NEXT.equalsIgnoreCase(attrName)) {
            ((Player) target).setNext((Player) value);
            return true;
        }
        return false;
    }

    public static IdMap createIdMap(String sessionID) {
        return de.uniks.abalone.model.util.CreatorCreator.createIdMap(sessionID);
    }

    // ==========================================================================
    public void removeObject(Object entity) {
        ((Player) entity).removeYou();
    }
}
