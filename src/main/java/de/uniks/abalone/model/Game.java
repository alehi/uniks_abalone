/*
 * Copyright (c) 2017 Olaf Versteeg Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.uniks.abalone.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import de.uniks.abalone.model.util.FieldSet;
import de.uniks.abalone.model.util.MoveSet;
import de.uniks.abalone.model.util.PlayerSet;
import de.uniks.abalone.model.util.TokenSet;
import de.uniks.networkparser.interfaces.SendableEntity;

/**
 *
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/abalone/modelgenerator/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/modelgenerator/abalone/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/modelgenerator/abalone/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/modelgenerator/abalone/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href='../../../../../../../src/test/java/de/uniks/modelgenerator/AbaloneModel.java'>AbaloneModel.java</a>
 */
public class Game implements SendableEntity {
    // ==========================================================================
    protected PropertyChangeSupport listeners = null;

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if(this.listeners != null) {
            this.listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    @Override
    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if(this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(listener);
        return true;
    }

    @Override
    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    @Override
    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if(this.listeners == null) {
            this.listeners.removePropertyChangeListener(listener);
        }
        this.listeners.removePropertyChangeListener(listener);
        return true;
    }

    @Override
    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(this.listeners != null) {
            this.listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    // ==========================================================================
    public void removeYou() {
        setBoard(null);
        withoutFields(getFields().toArray(new Field[getFields().size()]));
        withoutTokens(getTokens().toArray(new Token[getTokens().size()]));
        withoutPlayers(getPlayers().toArray(new Player[getPlayers().size()]));
        setCurrentPlayer(null);
        setWinner(null);
        setLogic(null);
        withoutHistory(this.getHistory().toArray(new Move[this.getHistory().size()]));
        firePropertyChange("REMOVE_YOU", this, null);
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Game ----------------------------------- Board
     *              game                   board
     * </pre>
     */
    public static final String PROPERTY_BOARD = "board";

    private Board board = null;

    public Board getBoard() {
        return this.board;
    }

    public boolean setBoard(Board value) {
        boolean changed = false;
        if(this.board != value) {
            Board oldValue = this.board;
            if(this.board != null) {
                this.board = null;
                oldValue.setGame(null);
            }
            this.board = value;
            if(value != null) {
                value.withGame(this);
            }
            firePropertyChange(PROPERTY_BOARD, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Game withBoard(Board value) {
        setBoard(value);
        return this;
    }

    public Board createBoard() {
        Board value = new Board();
        withBoard(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       many
     * Game ----------------------------------- Field
     *              game                   fields
     * </pre>
     */
    public static final String PROPERTY_FIELDS = "fields";

    private FieldSet fields = null;

    public FieldSet getFields() {
        if(this.fields == null) {
            return FieldSet.EMPTY_SET;
        }
        return this.fields;
    }

    public Game withFields(Field... value) {
        if(value == null) {
            return this;
        }
        for(Field item: value) {
            if(item != null) {
                if(this.fields == null) {
                    this.fields = new FieldSet();
                }
                boolean changed = this.fields.add(item);
                if(changed) {
                    item.withGame(this);
                    firePropertyChange(PROPERTY_FIELDS, null, item);
                }
            }
        }
        return this;
    }

    public Game withoutFields(Field... value) {
        for(Field item: value) {
            if((this.fields != null) && (item != null)) {
                if(this.fields.remove(item)) {
                    item.setGame(null);
                    firePropertyChange(PROPERTY_FIELDS, item, null);
                }
            }
        }
        return this;
    }

    public Field createFields() {
        Field value = new Field();
        withFields(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       many
     * Game ----------------------------------- Token
     *              game                   tokens
     * </pre>
     */
    public static final String PROPERTY_TOKENS = "tokens";

    private TokenSet tokens = null;

    public TokenSet getTokens() {
        if(this.tokens == null) {
            return TokenSet.EMPTY_SET;
        }
        return this.tokens;
    }

    public Game withTokens(Token... value) {
        if(value == null) {
            return this;
        }
        for(Token item: value) {
            if(item != null) {
                if(this.tokens == null) {
                    this.tokens = new TokenSet();
                }
                boolean changed = this.tokens.add(item);
                if(changed) {
                    item.withGame(this);
                    firePropertyChange(PROPERTY_TOKENS, null, item);
                }
            }
        }
        return this;
    }

    public Game withoutTokens(Token... value) {
        for(Token item: value) {
            if((this.tokens != null) && (item != null)) {
                if(this.tokens.remove(item)) {
                    item.setGame(null);
                    firePropertyChange(PROPERTY_TOKENS, item, null);
                }
            }
        }
        return this;
    }

    public Token createTokens() {
        Token value = new Token();
        withTokens(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       many
     * Game ----------------------------------- Player
     *              game                   players
     * </pre>
     */
    public static final String PROPERTY_PLAYERS = "players";

    private PlayerSet players = null;

    public PlayerSet getPlayers() {
        if(this.players == null) {
            return PlayerSet.EMPTY_SET;
        }
        return this.players;
    }

    public Game withPlayers(Player... value) {
        if(value == null) {
            return this;
        }
        for(Player item: value) {
            if(item != null) {
                if(this.players == null) {
                    this.players = new PlayerSet();
                }
                boolean changed = this.players.add(item);
                if(changed) {
                    item.withGame(this);
                    firePropertyChange(PROPERTY_PLAYERS, null, item);
                }
            }
        }
        return this;
    }

    public Game withoutPlayers(Player... value) {
        for(Player item: value) {
            if((this.players != null) && (item != null)) {
                if(this.players.remove(item)) {
                    item.setGame(null);
                    firePropertyChange(PROPERTY_PLAYERS, item, null);
                }
            }
        }
        return this;
    }

    public Player createPlayers() {
        Player value = new Player();
        withPlayers(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Game ----------------------------------- Player
     *              currentGame                   currentPlayer
     * </pre>
     */
    public static final String PROPERTY_CURRENTPLAYER = "currentPlayer";

    private Player currentPlayer = null;

    public Player getCurrentPlayer() {
        return this.currentPlayer;
    }

    public boolean setCurrentPlayer(Player value) {
        boolean changed = false;
        if(this.currentPlayer != value) {
            Player oldValue = this.currentPlayer;
            if(this.currentPlayer != null) {
                this.currentPlayer = null;
                oldValue.setCurrentGame(null);
            }
            this.currentPlayer = value;
            if(value != null) {
                value.withCurrentGame(this);
            }
            firePropertyChange(PROPERTY_CURRENTPLAYER, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Game withCurrentPlayer(Player value) {
        setCurrentPlayer(value);
        return this;
    }

    public Player createCurrentPlayer() {
        Player value = new Player();
        withCurrentPlayer(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Game ----------------------------------- Player
     *              wonGame                   winner
     * </pre>
     */
    public static final String PROPERTY_WINNER = "winner";

    private Player winner = null;

    public Player getWinner() {
        return this.winner;
    }

    public boolean setWinner(Player value) {
        boolean changed = false;
        if(this.winner != value) {
            Player oldValue = this.winner;
            if(this.winner != null) {
                this.winner = null;
                oldValue.setWonGame(null);
            }
            this.winner = value;
            if(value != null) {
                value.withWonGame(this);
            }
            firePropertyChange(PROPERTY_WINNER, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Game withWinner(Player value) {
        setWinner(value);
        return this;
    }

    public Player createWinner() {
        Player value = new Player();
        withWinner(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Game ----------------------------------- Logic
     *              game                   logic
     * </pre>
     */
    public static final String PROPERTY_LOGIC = "logic";

    private Logic logic = null;

    public Logic getLogic() {
        return this.logic;
    }

    public boolean setLogic(Logic value) {
        boolean changed = false;
        if(this.logic != value) {
            Logic oldValue = this.logic;
            if(this.logic != null) {
                this.logic = null;
                oldValue.setGame(null);
            }
            this.logic = value;
            if(value != null) {
                value.withGame(this);
            }
            firePropertyChange(PROPERTY_LOGIC, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Game withLogic(Logic value) {
        setLogic(value);
        return this;
    }

    public Logic createLogic() {
        Logic value = new Logic();
        withLogic(value);
        return value;
    }

    /**
     * Generates a board with fields and the associated token. Player "first"'s tokens are on the upper half. Player
     * "first" will start the game.
     *
     * @param first Player at the upper half
     * @param second Player at the lower half
     * @param tokenLayout The start configuration of the token
     */
    public void init(Player first, Player second, int[][] tokenLayout) {
        withPlayers(first, second);
        first.withNext(second);
        second.withNext(first);
        withCurrentPlayer(first);
        withLogic(new Logic());
        createBoard().init(first, second, tokenLayout);
    }

    public AIPlayer createPlayersAIPlayer() {
        AIPlayer value = new AIPlayer();
        withPlayers(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       many
     * Game ----------------------------------- Move
     *              game                   history
     * </pre>
     */
    public static final String PROPERTY_HISTORY = "history";

    private MoveSet history = null;

    public MoveSet getHistory() {
        if(this.history == null) {
            return MoveSet.EMPTY_SET;
        }

        return this.history;
    }

    public Game withHistory(Move... value) {
        if(value == null) {
            return this;
        }
        for(Move item: value) {
            if(item != null) {
                if(this.history == null) {
                    this.history = new MoveSet();
                }

                boolean changed = this.history.add(item);

                if(changed) {
                    firePropertyChange(PROPERTY_HISTORY, null, item);
                }
            }
        }
        return this;
    }

    public Game withoutHistory(Move... value) {
        for(Move item: value) {
            if((this.history != null) && (item != null)) {
                if(this.history.remove(item)) {
                    firePropertyChange(PROPERTY_HISTORY, item, null);
                }
            }
        }
        return this;
    }

    public Move createHistory() {
        Move value = new Move();
        withHistory(value);
        return value;
    }

    public void restoreSelectedBoard(Move move) {
        Move lastMove = null;

        // Remove all 'lastMoved' flags
        getFields().withLastMoved(false);

        while(this.history.last() != move && this.history.last() != null) {
            lastMove = this.history.last();
            getLogic().undoMove(lastMove);
        }
        if(this.history.last() == move && this.history.last() != null) {
            lastMove = this.history.last();
            getLogic().undoMove(lastMove);
        }
        if(lastMove != null) {
            setCurrentPlayer(lastMove.getTokens().get(0).getPlayer());
        }

        // Mark the fields of the last move as 'lastMoved'
        if(this.history.size() > 0) {
            lastMove = this.history.last();
            lastMove.getTokens().getField().withLastMoved(true);
        }
    }
}
