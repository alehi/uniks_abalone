/*
 * Copyright (c) 2017 Olaf Versteeg Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.uniks.abalone.model;

import de.uniks.ai.AI;
import javafx.application.Platform;

/**
 *
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/modelgenerator/abalone/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/modelgenerator/abalone/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/modelgenerator/abalone/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href='../../../../../../../src/test/java/de/uniks/modelgenerator/AbaloneModel.java'>AbaloneModel.java</a>
 */
public class AIPlayer extends Player {
    // ==========================================================================
    @Override
    public void removeYou() {
        setGame(null);
        setCurrentGame(null);
        setWonGame(null);
        withoutTokens(this.getTokens().toArray(new Token[this.getTokens().size()]));
        setPrevious(null);
        setNext(null);
        firePropertyChange("REMOVE_YOU", this, null);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(" ").append(this.getName());
        result.append(" ").append(this.getColor());
        result.append(" ").append(this.getScores());
        result.append(" ").append(this.getStrength());
        return result.substring(1);
    }

    // ==========================================================================
    public static final String PROPERTY_STRENGTH = "strength";

    private int strength;

    public int getStrength() {
        return this.strength;
    }

    public void setStrength(int value) {
        if(this.strength != value) {
            int oldValue = this.strength;
            this.strength = value;
            this.firePropertyChange(PROPERTY_STRENGTH, oldValue, value);
        }
    }

    public AIPlayer withStrength(int value) {
        setStrength(value);
        return this;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * AIPlayer ----------------------------------- de.uniks.ai.AI
     *              aiPlayer                   player
     * </pre>
     */
    private AI<Logic, Board, Move, Player> ai = null;

    public AI<Logic, Board, Move, Player> getAi() {
        return this.ai;
    }

    public boolean setAi(AI<Logic, Board, Move, Player> value) {
        boolean changed = false;
        if(this.ai != value) {
            this.ai = value;
        }
        return changed;
    }

    public AIPlayer withAi(AI<Logic, Board, Move, Player> value) {
        setAi(value);
        return this;
    }

    public void registerCurrentGameListener() {
        addPropertyChangeListener(Player.PROPERTY_CURRENTGAME, e -> {
            if(e.getNewValue() != null && getGame().getWinner() == null) {
                doYourMove();
            }
        });
    }

    public void doYourMove() {
        Player me = this;
        new Thread("AIThread") {
            // runnable for that thread
            @Override
            public void run() {
                Move bestMove = AIPlayer.this.ai.calculateBestMove(getGame().getLogic(), me, me.getNext(),
                        getGame().getBoard(),
                        AIPlayer.this.strength);
                // When the thread has been stopped during the calculation 'calculateBestMove' could return 'null',
                // depending on the AI type
                if(bestMove != null) {
                    Platform.runLater(() -> {
                        try {
                            getGame().getLogic().executeMove(bestMove);
                            getGame().setCurrentPlayer(getGame().getCurrentPlayer().getNext());

                        } catch(NullPointerException e) {
                            // ignore
                        }
                    });
                }
            }
        }.start();
    }
}
