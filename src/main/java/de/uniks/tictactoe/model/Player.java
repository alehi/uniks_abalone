/*
 * Copyright (c) 2017 Ich Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.uniks.tictactoe.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import de.uniks.ai.PlayerInterface;
import de.uniks.networkparser.EntityUtil;
import de.uniks.networkparser.interfaces.SendableEntity;
import de.uniks.tictactoe.model.util.PlayerSet;
import de.uniks.tictactoe.model.util.TokenSet;

/**
 * 
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/modelgenerator/tictactoe/ModelGenerator.java'>ModelGenerator.java</a>
 */
public class Player implements SendableEntity, PlayerInterface {

    // ==========================================================================

    protected PropertyChangeSupport listeners = null;

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if(listeners != null) {
            listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if(listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(listener);
        return true;
    }

    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if(listeners == null) {
            listeners.removePropertyChangeListener(listener);
        }
        listeners.removePropertyChangeListener(listener);
        return true;
    }

    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(listeners != null) {
            listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    // ==========================================================================

    public void removeYou() {
        setGame(null);
        setCurrentGame(null);
        setWonGame(null);
        withoutTokens(this.getTokens().toArray(new Token[this.getTokens().size()]));
        setPrevious(null);
        setNext(null);
        firePropertyChange("REMOVE_YOU", this, null);
    }

    // ==========================================================================

    public static final String PROPERTY_NAME = "name";

    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String value) {
        if(!EntityUtil.stringEquals(this.name, value)) {

            String oldValue = this.name;
            this.name = value;
            this.firePropertyChange(PROPERTY_NAME, oldValue, value);
        }
    }

    public Player withName(String value) {
        setName(value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append(" ").append(this.getName());
        result.append(" ").append(this.getSymbole());
        result.append(" ").append(this.getScore());
        return result.substring(1);
    }

    // ==========================================================================

    public static final String PROPERTY_SYMBOLE = "symbole";

    private String symbole;

    public String getSymbole() {
        return this.symbole;
    }

    public void setSymbole(String value) {
        if(!EntityUtil.stringEquals(this.symbole, value)) {

            String oldValue = this.symbole;
            this.symbole = value;
            this.firePropertyChange(PROPERTY_SYMBOLE, oldValue, value);
        }
    }

    public Player withSymbole(String value) {
        setSymbole(value);
        return this;
    }

    // ==========================================================================

    public static final String PROPERTY_SCORE = "score";

    private int score;

    public int getScore() {
        return this.score;
    }

    public void setScore(int value) {
        if(this.score != value) {

            int oldValue = this.score;
            this.score = value;
            this.firePropertyChange(PROPERTY_SCORE, oldValue, value);
        }
    }

    public Player withScore(int value) {
        setScore(value);
        return this;
    }

    /********************************************************************
     * <pre>
     *              many                       one
     * Player ----------------------------------- Game
     *              players                   game
     * </pre>
     */

    public static final String PROPERTY_GAME = "game";

    private Game game = null;

    public Game getGame() {
        return this.game;
    }

    public boolean setGame(Game value) {
        boolean changed = false;

        if(this.game != value) {
            Game oldValue = this.game;

            if(this.game != null) {
                this.game = null;
                oldValue.withoutPlayers(this);
            }

            this.game = value;

            if(value != null) {
                value.withPlayers(this);
            }

            firePropertyChange(PROPERTY_GAME, oldValue, value);
            changed = true;
        }

        return changed;
    }

    public Player withGame(Game value) {
        setGame(value);
        return this;
    }

    public Game createGame() {
        Game value = new Game();
        withGame(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Player ----------------------------------- Game
     *              currentPlayer                   currentGame
     * </pre>
     */

    public static final String PROPERTY_CURRENTGAME = "currentGame";

    private Game currentGame = null;

    public Game getCurrentGame() {
        return this.currentGame;
    }

    public boolean setCurrentGame(Game value) {
        boolean changed = false;

        if(this.currentGame != value) {
            Game oldValue = this.currentGame;

            if(this.currentGame != null) {
                this.currentGame = null;
                oldValue.setCurrentPlayer(null);
            }

            this.currentGame = value;

            if(value != null) {
                value.withCurrentPlayer(this);
            }

            firePropertyChange(PROPERTY_CURRENTGAME, oldValue, value);
            changed = true;
        }

        return changed;
    }

    public Player withCurrentGame(Game value) {
        setCurrentGame(value);
        return this;
    }

    public Game createCurrentGame() {
        Game value = new Game();
        withCurrentGame(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Player ----------------------------------- Game
     *              winner                   wonGame
     * </pre>
     */

    public static final String PROPERTY_WONGAME = "wonGame";

    private Game wonGame = null;

    public Game getWonGame() {
        return this.wonGame;
    }

    public boolean setWonGame(Game value) {
        boolean changed = false;

        if(this.wonGame != value) {
            Game oldValue = this.wonGame;

            if(this.wonGame != null) {
                this.wonGame = null;
                oldValue.setWinner(null);
            }

            this.wonGame = value;

            if(value != null) {
                value.withWinner(this);
            }

            firePropertyChange(PROPERTY_WONGAME, oldValue, value);
            changed = true;
        }

        return changed;
    }

    public Player withWonGame(Game value) {
        setWonGame(value);
        return this;
    }

    public Game createWonGame() {
        Game value = new Game();
        withWonGame(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       many
     * Player ----------------------------------- Token
     *              player                   tokens
     * </pre>
     */

    public static final String PROPERTY_TOKENS = "tokens";

    private TokenSet tokens = null;

    public TokenSet getTokens() {
        if(this.tokens == null) {
            return TokenSet.EMPTY_SET;
        }

        return this.tokens;
    }

    public Player withTokens(Token... value) {
        if(value == null) {
            return this;
        }
        for(Token item: value) {
            if(item != null) {
                if(this.tokens == null) {
                    this.tokens = new TokenSet();
                }

                boolean changed = this.tokens.add(item);

                if(changed) {
                    item.withPlayer(this);
                    firePropertyChange(PROPERTY_TOKENS, null, item);
                }
            }
        }
        return this;
    }

    public Player withoutTokens(Token... value) {
        for(Token item: value) {
            if((this.tokens != null) && (item != null)) {
                if(this.tokens.remove(item)) {
                    item.setPlayer(null);
                    firePropertyChange(PROPERTY_TOKENS, item, null);
                }
            }
        }
        return this;
    }

    public Token createTokens() {
        Token value = new Token();
        withTokens(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Player ----------------------------------- Player
     *              next                   previous
     * </pre>
     */

    public static final String PROPERTY_PREVIOUS = "previous";

    private Player previous = null;

    public Player getPrevious() {
        return this.previous;
    }

    public PlayerSet getPreviousTransitive() {
        PlayerSet result = new PlayerSet().with(this);
        return result.getPreviousTransitive();
    }

    public boolean setPrevious(Player value) {
        boolean changed = false;

        if(this.previous != value) {
            Player oldValue = this.previous;

            if(this.previous != null) {
                this.previous = null;
                oldValue.setNext(null);
            }

            this.previous = value;

            if(value != null) {
                value.withNext(this);
            }

            firePropertyChange(PROPERTY_PREVIOUS, oldValue, value);
            changed = true;
        }

        return changed;
    }

    public Player withPrevious(Player value) {
        setPrevious(value);
        return this;
    }

    public Player createPrevious() {
        Player value = new Player();
        withPrevious(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Player ----------------------------------- Player
     *              previous                   next
     * </pre>
     */

    public static final String PROPERTY_NEXT = "next";

    private Player next = null;

    public Player getNext() {
        return this.next;
    }

    public PlayerSet getNextTransitive() {
        PlayerSet result = new PlayerSet().with(this);
        return result.getNextTransitive();
    }

    public boolean setNext(Player value) {
        boolean changed = false;

        if(this.next != value) {
            Player oldValue = this.next;

            if(this.next != null) {
                this.next = null;
                oldValue.setPrevious(null);
            }

            this.next = value;

            if(value != null) {
                value.withPrevious(this);
            }

            firePropertyChange(PROPERTY_NEXT, oldValue, value);
            changed = true;
        }

        return changed;
    }

    public Player withNext(Player value) {
        setNext(value);
        return this;
    }

    public Player createNext() {
        Player value = new Player();
        withNext(value);
        return value;
    }
}
