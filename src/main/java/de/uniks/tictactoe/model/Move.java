/*
 * Copyright (c) 2017 Thorsten Otto Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.uniks.tictactoe.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import de.uniks.ai.MoveInterface;
import de.uniks.networkparser.interfaces.SendableEntity;

/**
 * 
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/modelgenerator/tictactoe/ModelGenerator.java'>ModelGenerator.java</a>
 */
public class Move implements SendableEntity, MoveInterface<Move> {
    protected PropertyChangeSupport listeners = null;

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if(listeners != null) {
            listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    @Override
    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if(listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(listener);
        return true;
    }

    @Override
    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    @Override
    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if(listeners == null) {
            listeners.removePropertyChangeListener(listener);
        }
        listeners.removePropertyChangeListener(listener);
        return true;
    }

    @Override
    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(listeners != null) {
            listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    // ==========================================================================
    public void removeYou() {
        setField(null);
        setPlayer(null);
        firePropertyChange("REMOVE_YOU", this, null);
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Move ----------------------------------- Field
     *              move                   field
     * </pre>
     */
    public static final String PROPERTY_FIELD = "field";

    private Field field = null;

    public Field getField() {
        return this.field;
    }

    public boolean setField(Field value) {
        boolean changed = false;
        if(this.field != value) {
            Field oldValue = this.field;
            this.field = value;
            firePropertyChange(PROPERTY_FIELD, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Move withField(Field value) {
        setField(value);
        return this;
    }

    public Field createField() {
        Field value = new Field();
        withField(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Move ----------------------------------- Player
     *              move                   player
     * </pre>
     */
    public static final String PROPERTY_PLAYER = "player";

    private Player player = null;

    public Player getPlayer() {
        return this.player;
    }

    public boolean setPlayer(Player value) {
        boolean changed = false;
        if(this.player != value) {
            Player oldValue = this.player;
            this.player = value;
            firePropertyChange(PROPERTY_PLAYER, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Move withPlayer(Player value) {
        setPlayer(value);
        return this;
    }

    public Player createPlayer() {
        Player value = new Player();
        withPlayer(value);
        return value;
    }

    @Override
    public String toString() {
        return "(" + this.getField().getX() + ", " + this.getField().getY() + ")";
    }

    @Override
    public boolean hitMove() {
        return false;
    }

    /**
     * Compares two moves by checking the fields positions on the board.
     * 
     * @param Move is an object of the Tic Tac Toe Move class.
     * @return Returns 1, if move1 is less than move2. -1, if move1 is greater than move2. 0, if they are equal.
     */
    @Override
    public int compareTo(Move move) {
        int thisMove = evaluateMove(this);
        int otherMove = evaluateMove(move);
        if(thisMove < otherMove) {
            return 1;
        } else if(thisMove > otherMove) {
            return -1;
        } else {
            return 0;
        }
    }

    // Gives a given move a value. The field in the middle gets the value 3. The corners 2 and the one between 1.

    // 2 | 1 | 2
    // 1 | 3 | 1
    // 2 | 1 | 2

    // return 0, if they have the same score.

    private int evaluateMove(Move move) {
        if(move.getField().getX() == 1 && move.getField().getY() == 1) {
            return 3;
        }
        if(move.getField().getX() == 0 && move.getField().getY() == 0
                || move.getField().getX() == 2 && move.getField().getY() == 2
                || move.getField().getX() == 0 && move.getField().getY() == 2
                || move.getField().getX() == 2 && move.getField().getY() == 0) {
            return 2;
        }
        if(move.getField().getX() == 0 && move.getField().getY() == 1
                || move.getField().getX() == 1 && move.getField().getY() == 0
                || move.getField().getX() == 1 && move.getField().getY() == 2
                || move.getField().getX() == 2 && move.getField().getY() == 1) {
            return 1;
        }
        return 0;
    }

    /**
     * Compares two Tic Tac Toe moves by checking if their fields have the same coordinates.
     * 
     * @param move is an object of the Tic Tac Toe Move class.
     * @return true, if both moves are equal. False, if they are not.
     */
    @Override
    public boolean equals(Move move) {

        // Check, if the fields of the moves have the same coordinates.
        if(this.getField().getX() == move.getField().getX() &&
                this.getField().getY() == move.getField().getY()) {
            return true;
        }

        return false;
    }
}
