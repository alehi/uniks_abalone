package de.uniks.tictactoe.model.util;

import org.sdmlib.models.pattern.util.PatternObjectCreator;

import de.uniks.networkparser.IdMap;
import de.uniks.tictactoe.model.Board;

public class BoardPOCreator extends PatternObjectCreator {
    @Override
    public Object getSendableInstance(boolean reference) {
        if(reference) {
            return new BoardPO(new Board[] {});
        } else {
            return new BoardPO();
        }
    }

    public static IdMap createIdMap(String sessionID) {
        return de.uniks.tictactoe.model.util.CreatorCreator.createIdMap(sessionID);
    }
}
