package de.uniks.tictactoe.model.util;

import org.sdmlib.models.pattern.AttributeConstraint;
import org.sdmlib.models.pattern.Pattern;
import org.sdmlib.models.pattern.PatternObject;

import de.uniks.tictactoe.model.AIPlayer;
import de.uniks.tictactoe.model.Game;
import de.uniks.tictactoe.model.Player;
import de.uniks.tictactoe.model.Token;

public class AIPlayerPO extends PatternObject<AIPlayerPO, AIPlayer> {

    public AIPlayerSet allMatches() {
        this.setDoAllMatches(true);

        AIPlayerSet matches = new AIPlayerSet();

        while(this.getPattern().getHasMatch()) {
            matches.add(this.getCurrentMatch());

            this.getPattern().findMatch();
        }

        return matches;
    }

    public AIPlayerPO() {
        newInstance(null);
    }

    public AIPlayerPO(AIPlayer... hostGraphObject) {
        if(hostGraphObject == null || hostGraphObject.length < 1) {
            return;
        }
        newInstance(null, hostGraphObject);
    }

    public AIPlayerPO(String modifier) {
        this.setModifier(modifier);
    }

    public AIPlayerPO createStrengthCondition(int value) {
        new AttributeConstraint()
                .withAttrName(AIPlayer.PROPERTY_STRENGTH)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public AIPlayerPO createStrengthCondition(int lower, int upper) {
        new AttributeConstraint()
                .withAttrName(AIPlayer.PROPERTY_STRENGTH)
                .withTgtValue(lower)
                .withUpperTgtValue(upper)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public AIPlayerPO createStrengthAssignment(int value) {
        new AttributeConstraint()
                .withAttrName(AIPlayer.PROPERTY_STRENGTH)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(Pattern.CREATE)
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public int getStrength() {
        if(this.getPattern().getHasMatch()) {
            return getCurrentMatch().getStrength();
        }
        return 0;
    }

    public AIPlayerPO withStrength(int value) {
        if(this.getPattern().getHasMatch()) {
            getCurrentMatch().setStrength(value);
        }
        return this;
    }

    public AIPlayerPO createNameCondition(String value) {
        new AttributeConstraint()
                .withAttrName(AIPlayer.PROPERTY_NAME)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public AIPlayerPO createNameCondition(String lower, String upper) {
        new AttributeConstraint()
                .withAttrName(AIPlayer.PROPERTY_NAME)
                .withTgtValue(lower)
                .withUpperTgtValue(upper)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public AIPlayerPO createNameAssignment(String value) {
        new AttributeConstraint()
                .withAttrName(AIPlayer.PROPERTY_NAME)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(Pattern.CREATE)
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public String getName() {
        if(this.getPattern().getHasMatch()) {
            return getCurrentMatch().getName();
        }
        return null;
    }

    public AIPlayerPO withName(String value) {
        if(this.getPattern().getHasMatch()) {
            getCurrentMatch().setName(value);
        }
        return this;
    }

    public AIPlayerPO createSymboleCondition(String value) {
        new AttributeConstraint()
                .withAttrName(AIPlayer.PROPERTY_SYMBOLE)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public AIPlayerPO createSymboleCondition(String lower, String upper) {
        new AttributeConstraint()
                .withAttrName(AIPlayer.PROPERTY_SYMBOLE)
                .withTgtValue(lower)
                .withUpperTgtValue(upper)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public AIPlayerPO createSymboleAssignment(String value) {
        new AttributeConstraint()
                .withAttrName(AIPlayer.PROPERTY_SYMBOLE)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(Pattern.CREATE)
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public String getSymbole() {
        if(this.getPattern().getHasMatch()) {
            return getCurrentMatch().getSymbole();
        }
        return null;
    }

    public AIPlayerPO withSymbole(String value) {
        if(this.getPattern().getHasMatch()) {
            getCurrentMatch().setSymbole(value);
        }
        return this;
    }

    public AIPlayerPO createScoreCondition(int value) {
        new AttributeConstraint()
                .withAttrName(AIPlayer.PROPERTY_SCORE)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public AIPlayerPO createScoreCondition(int lower, int upper) {
        new AttributeConstraint()
                .withAttrName(AIPlayer.PROPERTY_SCORE)
                .withTgtValue(lower)
                .withUpperTgtValue(upper)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public AIPlayerPO createScoreAssignment(int value) {
        new AttributeConstraint()
                .withAttrName(AIPlayer.PROPERTY_SCORE)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(Pattern.CREATE)
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public int getScore() {
        if(this.getPattern().getHasMatch()) {
            return getCurrentMatch().getScore();
        }
        return 0;
    }

    public AIPlayerPO withScore(int value) {
        if(this.getPattern().getHasMatch()) {
            getCurrentMatch().setScore(value);
        }
        return this;
    }

    public GamePO createGamePO() {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(AIPlayer.PROPERTY_GAME, result);

        return result;
    }

    public GamePO createGamePO(String modifier) {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(modifier);
        super.hasLink(AIPlayer.PROPERTY_GAME, result);

        return result;
    }

    public AIPlayerPO createGameLink(GamePO tgt) {
        return hasLinkConstraint(tgt, AIPlayer.PROPERTY_GAME);
    }

    public AIPlayerPO createGameLink(GamePO tgt, String modifier) {
        return hasLinkConstraint(tgt, AIPlayer.PROPERTY_GAME, modifier);
    }

    public Game getGame() {
        if(this.getPattern().getHasMatch()) {
            return this.getCurrentMatch().getGame();
        }
        return null;
    }

    public GamePO createCurrentGamePO() {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(AIPlayer.PROPERTY_CURRENTGAME, result);

        return result;
    }

    public GamePO createCurrentGamePO(String modifier) {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(modifier);
        super.hasLink(AIPlayer.PROPERTY_CURRENTGAME, result);

        return result;
    }

    public AIPlayerPO createCurrentGameLink(GamePO tgt) {
        return hasLinkConstraint(tgt, AIPlayer.PROPERTY_CURRENTGAME);
    }

    public AIPlayerPO createCurrentGameLink(GamePO tgt, String modifier) {
        return hasLinkConstraint(tgt, AIPlayer.PROPERTY_CURRENTGAME, modifier);
    }

    public Game getCurrentGame() {
        if(this.getPattern().getHasMatch()) {
            return this.getCurrentMatch().getCurrentGame();
        }
        return null;
    }

    public GamePO createWonGamePO() {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(AIPlayer.PROPERTY_WONGAME, result);

        return result;
    }

    public GamePO createWonGamePO(String modifier) {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(modifier);
        super.hasLink(AIPlayer.PROPERTY_WONGAME, result);

        return result;
    }

    public AIPlayerPO createWonGameLink(GamePO tgt) {
        return hasLinkConstraint(tgt, AIPlayer.PROPERTY_WONGAME);
    }

    public AIPlayerPO createWonGameLink(GamePO tgt, String modifier) {
        return hasLinkConstraint(tgt, AIPlayer.PROPERTY_WONGAME, modifier);
    }

    public Game getWonGame() {
        if(this.getPattern().getHasMatch()) {
            return this.getCurrentMatch().getWonGame();
        }
        return null;
    }

    public TokenPO createTokensPO() {
        TokenPO result = new TokenPO(new Token[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(AIPlayer.PROPERTY_TOKENS, result);

        return result;
    }

    public TokenPO createTokensPO(String modifier) {
        TokenPO result = new TokenPO(new Token[] {});

        result.setModifier(modifier);
        super.hasLink(AIPlayer.PROPERTY_TOKENS, result);

        return result;
    }

    public AIPlayerPO createTokensLink(TokenPO tgt) {
        return hasLinkConstraint(tgt, AIPlayer.PROPERTY_TOKENS);
    }

    public AIPlayerPO createTokensLink(TokenPO tgt, String modifier) {
        return hasLinkConstraint(tgt, AIPlayer.PROPERTY_TOKENS, modifier);
    }

    public TokenSet getTokens() {
        if(this.getPattern().getHasMatch()) {
            return this.getCurrentMatch().getTokens();
        }
        return null;
    }

    public PlayerPO createPreviousPO() {
        PlayerPO result = new PlayerPO(new Player[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(AIPlayer.PROPERTY_PREVIOUS, result);

        return result;
    }

    public PlayerPO createPreviousPO(String modifier) {
        PlayerPO result = new PlayerPO(new Player[] {});

        result.setModifier(modifier);
        super.hasLink(AIPlayer.PROPERTY_PREVIOUS, result);

        return result;
    }

    public AIPlayerPO createPreviousLink(PlayerPO tgt) {
        return hasLinkConstraint(tgt, AIPlayer.PROPERTY_PREVIOUS);
    }

    public AIPlayerPO createPreviousLink(PlayerPO tgt, String modifier) {
        return hasLinkConstraint(tgt, AIPlayer.PROPERTY_PREVIOUS, modifier);
    }

    public Player getPrevious() {
        if(this.getPattern().getHasMatch()) {
            return this.getCurrentMatch().getPrevious();
        }
        return null;
    }

    public PlayerPO createNextPO() {
        PlayerPO result = new PlayerPO(new Player[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(AIPlayer.PROPERTY_NEXT, result);

        return result;
    }

    public PlayerPO createNextPO(String modifier) {
        PlayerPO result = new PlayerPO(new Player[] {});

        result.setModifier(modifier);
        super.hasLink(AIPlayer.PROPERTY_NEXT, result);

        return result;
    }

    public AIPlayerPO createNextLink(PlayerPO tgt) {
        return hasLinkConstraint(tgt, AIPlayer.PROPERTY_NEXT);
    }

    public AIPlayerPO createNextLink(PlayerPO tgt, String modifier) {
        return hasLinkConstraint(tgt, AIPlayer.PROPERTY_NEXT, modifier);
    }

    public Player getNext() {
        if(this.getPattern().getHasMatch()) {
            return this.getCurrentMatch().getNext();
        }
        return null;
    }
}
