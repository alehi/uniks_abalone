package de.uniks.tictactoe.model.util;

import org.sdmlib.models.pattern.PatternObject;

import de.uniks.tictactoe.model.Board;
import de.uniks.tictactoe.model.Field;
import de.uniks.tictactoe.model.Game;

public class BoardPO extends PatternObject<BoardPO, Board> {

    public BoardSet allMatches() {
        this.setDoAllMatches(true);

        BoardSet matches = new BoardSet();

        while(this.getPattern().getHasMatch()) {
            matches.add((Board) this.getCurrentMatch());

            this.getPattern().findMatch();
        }

        return matches;
    }

    public BoardPO() {
        newInstance(null);
    }

    public BoardPO(Board... hostGraphObject) {
        if(hostGraphObject == null || hostGraphObject.length < 1) {
            return;
        }
        newInstance(null, hostGraphObject);
    }

    public BoardPO(String modifier) {
        this.setModifier(modifier);
    }

    public GamePO createGamePO() {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Board.PROPERTY_GAME, result);

        return result;
    }

    public GamePO createGamePO(String modifier) {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(modifier);
        super.hasLink(Board.PROPERTY_GAME, result);

        return result;
    }

    public BoardPO createGameLink(GamePO tgt) {
        return hasLinkConstraint(tgt, Board.PROPERTY_GAME);
    }

    public BoardPO createGameLink(GamePO tgt, String modifier) {
        return hasLinkConstraint(tgt, Board.PROPERTY_GAME, modifier);
    }

    public Game getGame() {
        if(this.getPattern().getHasMatch()) {
            return ((Board) this.getCurrentMatch()).getGame();
        }
        return null;
    }

    public FieldPO createFieldsPO() {
        FieldPO result = new FieldPO(new Field[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Board.PROPERTY_FIELDS, result);

        return result;
    }

    public FieldPO createFieldsPO(String modifier) {
        FieldPO result = new FieldPO(new Field[] {});

        result.setModifier(modifier);
        super.hasLink(Board.PROPERTY_FIELDS, result);

        return result;
    }

    public BoardPO createFieldsLink(FieldPO tgt) {
        return hasLinkConstraint(tgt, Board.PROPERTY_FIELDS);
    }

    public BoardPO createFieldsLink(FieldPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Board.PROPERTY_FIELDS, modifier);
    }

    public FieldSet getFields() {
        if(this.getPattern().getHasMatch()) {
            return ((Board) this.getCurrentMatch()).getFields();
        }
        return null;
    }

}
