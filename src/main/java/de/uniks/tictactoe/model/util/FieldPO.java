package de.uniks.tictactoe.model.util;

import org.sdmlib.models.pattern.AttributeConstraint;
import org.sdmlib.models.pattern.Pattern;
import org.sdmlib.models.pattern.PatternObject;

import de.uniks.tictactoe.model.Board;
import de.uniks.tictactoe.model.Field;
import de.uniks.tictactoe.model.Game;
import de.uniks.tictactoe.model.Token;

public class FieldPO extends PatternObject<FieldPO, Field> {

    public FieldSet allMatches() {
        this.setDoAllMatches(true);

        FieldSet matches = new FieldSet();

        while(this.getPattern().getHasMatch()) {
            matches.add((Field) this.getCurrentMatch());

            this.getPattern().findMatch();
        }

        return matches;
    }

    public FieldPO() {
        newInstance(null);
    }

    public FieldPO(Field... hostGraphObject) {
        if(hostGraphObject == null || hostGraphObject.length < 1) {
            return;
        }
        newInstance(null, hostGraphObject);
    }

    public FieldPO(String modifier) {
        this.setModifier(modifier);
    }

    public FieldPO createXCondition(int value) {
        new AttributeConstraint()
                .withAttrName(Field.PROPERTY_X)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public FieldPO createXCondition(int lower, int upper) {
        new AttributeConstraint()
                .withAttrName(Field.PROPERTY_X)
                .withTgtValue(lower)
                .withUpperTgtValue(upper)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public FieldPO createXAssignment(int value) {
        new AttributeConstraint()
                .withAttrName(Field.PROPERTY_X)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(Pattern.CREATE)
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public int getX() {
        if(this.getPattern().getHasMatch()) {
            return ((Field) getCurrentMatch()).getX();
        }
        return 0;
    }

    public FieldPO withX(int value) {
        if(this.getPattern().getHasMatch()) {
            ((Field) getCurrentMatch()).setX(value);
        }
        return this;
    }

    public FieldPO createYCondition(int value) {
        new AttributeConstraint()
                .withAttrName(Field.PROPERTY_Y)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public FieldPO createYCondition(int lower, int upper) {
        new AttributeConstraint()
                .withAttrName(Field.PROPERTY_Y)
                .withTgtValue(lower)
                .withUpperTgtValue(upper)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public FieldPO createYAssignment(int value) {
        new AttributeConstraint()
                .withAttrName(Field.PROPERTY_Y)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(Pattern.CREATE)
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public int getY() {
        if(this.getPattern().getHasMatch()) {
            return ((Field) getCurrentMatch()).getY();
        }
        return 0;
    }

    public FieldPO withY(int value) {
        if(this.getPattern().getHasMatch()) {
            ((Field) getCurrentMatch()).setY(value);
        }
        return this;
    }

    public GamePO createGamePO() {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Field.PROPERTY_GAME, result);

        return result;
    }

    public GamePO createGamePO(String modifier) {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(modifier);
        super.hasLink(Field.PROPERTY_GAME, result);

        return result;
    }

    public FieldPO createGameLink(GamePO tgt) {
        return hasLinkConstraint(tgt, Field.PROPERTY_GAME);
    }

    public FieldPO createGameLink(GamePO tgt, String modifier) {
        return hasLinkConstraint(tgt, Field.PROPERTY_GAME, modifier);
    }

    public Game getGame() {
        if(this.getPattern().getHasMatch()) {
            return ((Field) this.getCurrentMatch()).getGame();
        }
        return null;
    }

    public BoardPO createBoardPO() {
        BoardPO result = new BoardPO(new Board[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Field.PROPERTY_BOARD, result);

        return result;
    }

    public BoardPO createBoardPO(String modifier) {
        BoardPO result = new BoardPO(new Board[] {});

        result.setModifier(modifier);
        super.hasLink(Field.PROPERTY_BOARD, result);

        return result;
    }

    public FieldPO createBoardLink(BoardPO tgt) {
        return hasLinkConstraint(tgt, Field.PROPERTY_BOARD);
    }

    public FieldPO createBoardLink(BoardPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Field.PROPERTY_BOARD, modifier);
    }

    public Board getBoard() {
        if(this.getPattern().getHasMatch()) {
            return ((Field) this.getCurrentMatch()).getBoard();
        }
        return null;
    }

    public TokenPO createTokenPO() {
        TokenPO result = new TokenPO(new Token[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Field.PROPERTY_TOKEN, result);

        return result;
    }

    public TokenPO createTokenPO(String modifier) {
        TokenPO result = new TokenPO(new Token[] {});

        result.setModifier(modifier);
        super.hasLink(Field.PROPERTY_TOKEN, result);

        return result;
    }

    public FieldPO createTokenLink(TokenPO tgt) {
        return hasLinkConstraint(tgt, Field.PROPERTY_TOKEN);
    }

    public FieldPO createTokenLink(TokenPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Field.PROPERTY_TOKEN, modifier);
    }

    public Token getToken() {
        if(this.getPattern().getHasMatch()) {
            return ((Field) this.getCurrentMatch()).getToken();
        }
        return null;
    }

}
