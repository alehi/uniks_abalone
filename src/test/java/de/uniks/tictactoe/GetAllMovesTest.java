package de.uniks.tictactoe;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import de.uniks.tictactoe.model.Game;
import de.uniks.tictactoe.model.Logic;
import de.uniks.tictactoe.model.Move;
import de.uniks.tictactoe.model.Player;

/**
 * @author Thorsten Otto
 * @date 18.04.2017
 */

public class GetAllMovesTest {

    @Test
    public void getAllMovesOnEmptyBoardTest() {

        /*
         * AI = X Player = O Board situation | | | | | | | | | | | |
         */

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        ArrayList<Move> allMovesForPlayerOne = logic.getAllMoves(player1, game.getBoard());
        ArrayList<Move> allMovesForPlayerTwo = logic.getAllMoves(player2, game.getBoard());

        assertEquals(9, allMovesForPlayerOne.size());
        assertEquals(9, allMovesForPlayerTwo.size());

    }

    @Test
    public void getAllMovesOnFullBoardTest() {

        /*
         * AI = X Player = O Board situation | X | X | O | | O | O | X | | X | O | X |
         */

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        Move move1 = new Move().withField(game.getBoard().getFields().get(0)).withPlayer(player1);
        Move move2 = new Move().withField(game.getBoard().getFields().get(1)).withPlayer(player1);
        Move move3 = new Move().withField(game.getBoard().getFields().get(2)).withPlayer(player2);
        Move move4 = new Move().withField(game.getBoard().getFields().get(3)).withPlayer(player2);
        Move move5 = new Move().withField(game.getBoard().getFields().get(4)).withPlayer(player2);
        Move move6 = new Move().withField(game.getBoard().getFields().get(5)).withPlayer(player1);
        Move move7 = new Move().withField(game.getBoard().getFields().get(6)).withPlayer(player1);
        Move move8 = new Move().withField(game.getBoard().getFields().get(7)).withPlayer(player2);
        Move move9 = new Move().withField(game.getBoard().getFields().get(8)).withPlayer(player1);

        logic.executeMove(move1);
        logic.executeMove(move2);
        logic.executeMove(move3);
        logic.executeMove(move4);
        logic.executeMove(move5);
        logic.executeMove(move6);
        logic.executeMove(move7);
        logic.executeMove(move8);
        logic.executeMove(move9);

        ArrayList<Move> allMovesForPlayerOne = logic.getAllMoves(player1, game.getBoard());
        ArrayList<Move> allMovesForPlayerTwo = logic.getAllMoves(player2, game.getBoard());

        assertEquals(0, allMovesForPlayerOne.size());
        assertEquals(0, allMovesForPlayerTwo.size());
    }

    @Test
    public void getAllMovesWhenThereIsAWinnerTest() {

        /*
         * AI = X Player = O Board situation | X | X | X | | O | O | | | | | |
         */

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        Move move1 = new Move().withField(game.getBoard().getFields().get(0)).withPlayer(player1);
        Move move2 = new Move().withField(game.getBoard().getFields().get(1)).withPlayer(player1);
        Move move3 = new Move().withField(game.getBoard().getFields().get(2)).withPlayer(player1);
        Move move4 = new Move().withField(game.getBoard().getFields().get(3)).withPlayer(player2);
        Move move5 = new Move().withField(game.getBoard().getFields().get(4)).withPlayer(player2);

        logic.executeMove(move1);
        logic.executeMove(move2);
        logic.executeMove(move3);
        logic.executeMove(move4);
        logic.executeMove(move5);

        ArrayList<Move> allMovesForPlayerOne = logic.getAllMoves(player1, game.getBoard());
        ArrayList<Move> allMovesForPlayerTwo = logic.getAllMoves(player2, game.getBoard());

        assertEquals(0, allMovesForPlayerOne.size());
        assertEquals(0, allMovesForPlayerTwo.size());
    }
}
