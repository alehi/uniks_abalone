package de.uniks.tictactoe;

import static org.junit.Assert.*;

import org.junit.Test;

import de.uniks.tictactoe.model.Field;
import de.uniks.tictactoe.model.Move;

public class TTTMoveEqualsTest {

    @Test
    public void TwoMovesAreEqualTest() {

        Move move1 = new Move();
        Field field1 = new Field().withX(2).withY(1);
        move1.withField(field1);

        Move move2 = new Move();
        Field field2 = new Field().withX(2).withY(1);
        move2.withField(field2);

        assertTrue(move1.equals(move2));
    }

    @Test
    public void TwoMovesAreNotEqualTest() {

        Move move1 = new Move();
        Field field1 = new Field().withX(2).withY(1);
        move1.withField(field1);

        Move move2 = new Move();
        Field field2 = new Field().withX(2).withY(2);
        move2.withField(field2);

        assertFalse(move1.equals(move2));
    }

}
