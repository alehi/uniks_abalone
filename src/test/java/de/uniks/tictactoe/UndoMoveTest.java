package de.uniks.tictactoe;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import de.uniks.tictactoe.model.Field;
import de.uniks.tictactoe.model.Game;
import de.uniks.tictactoe.model.Logic;
import de.uniks.tictactoe.model.Move;
import de.uniks.tictactoe.model.Player;

/**
 * @author Thorsten Otto
 * @date 26.04.2017
 */

public class UndoMoveTest {

    @Test
    public void undoMoveOnEmptyBoardTest() {

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        Move move = new Move().withPlayer(player1);
        move.withField(game.getBoard().getFields().get(0));

        int tokenCountBeforeUndoMove = 0;

        for(Field field: game.getBoard().getFields()) {
            if(field.getToken() != null) {
                tokenCountBeforeUndoMove += 1;
            }
        }

        logic.undoMove(move);

        int tokenCountAfterUndoMove = 0;

        for(Field field: game.getBoard().getFields()) {
            if(field.getToken() != null) {
                tokenCountAfterUndoMove += 1;
            }
        }

        assertEquals(tokenCountBeforeUndoMove, tokenCountAfterUndoMove);
    }

    @Test
    public void undoMoveFirstFieldTest() {

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        Move move = new Move().withPlayer(player1);
        move.withField(game.getBoard().getFields().get(0));

        logic.executeMove(move);
        logic.undoMove(move);

        assertEquals(null, game.getBoard().getFields().get(0).getToken());
    }

    @Test
    public void undoMoveLastFieldTest() {

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        Move firstMove = new Move().withPlayer(player1);
        firstMove.withField(game.getBoard().getFields().get(0));
        logic.executeMove(firstMove);

        Move secondMove = new Move().withPlayer(player2);
        secondMove.withField(game.getBoard().getFields().get(1));
        logic.executeMove(secondMove);

        Move thirdMove = new Move().withPlayer(player1);
        thirdMove.withField(game.getBoard().getFields().get(3));
        logic.executeMove(thirdMove);

        Move fourthMove = new Move().withPlayer(player2);
        fourthMove.withField(game.getBoard().getFields().get(7));
        logic.executeMove(fourthMove);

        logic.undoMove(fourthMove);

        assertEquals(player1, game.getBoard().getFields().get(0).getToken().getPlayer());
        assertEquals(player2, game.getBoard().getFields().get(1).getToken().getPlayer());
        assertEquals(player1, game.getBoard().getFields().get(3).getToken().getPlayer());
        assertEquals(null, game.getBoard().getFields().get(7).getToken());
    }

    @Test
    public void wrongPlayerUndoMoveTest() {

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        Move firstMove = new Move().withPlayer(player1);
        firstMove.withField(game.getBoard().getFields().get(0));
        logic.executeMove(firstMove);

        int tokenCountBeforeUndoMove = 0;

        for(Field field: game.getBoard().getFields()) {
            if(field.getToken() != null) {
                tokenCountBeforeUndoMove += 1;
            }
        }

        logic.undoMove(firstMove);

        int tokenCountAfterUndoMove = 0;
        for(Field field: game.getBoard().getFields()) {
            if(field.getToken() != null) {
                tokenCountAfterUndoMove += 1;
            }
        }

        assertNotEquals(tokenCountBeforeUndoMove, tokenCountAfterUndoMove);
    }

}
