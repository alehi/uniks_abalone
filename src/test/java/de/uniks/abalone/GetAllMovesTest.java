package de.uniks.abalone;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Move;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.tools.Layouts;

public class GetAllMovesTest {
    @Test
    public void allMoves1() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O O O O
        4     O O O O O O O O O
        5      O O O O W O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        Player p1 = new Player().withColor("black").withName("Alice");
        Player p2 = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(p1, p2, tokenLayout);
        assertEquals(0, game.getLogic().getAllMoves(p1, game.getBoard()).size());
        assertEquals(6, game.getLogic().getAllMoves(p2, game.getBoard()).size());
    }

    @Test
    public void allMoves2() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O W O O O O
        4     O O O O W O O O O
        5      O O O O W O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        Player p1 = new Player().withColor("black").withName("Alice");
        Player p2 = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(p1, p2, tokenLayout);
        ArrayList<Move> allMoves = game.getLogic().getAllMoves(p2, game.getBoard());
        for(Move move: allMoves) {
            System.out.println(move);
        }
        assertEquals(30, allMoves.size());
    }

    @Test
    public void allMoves3() {
        Player p1 = new Player().withColor("black").withName("Alice");
        Player p2 = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(p1, p2, Layouts.BELGIAN_DAISY_TOKEN_LAYOUT);
        ArrayList<Move> allMoves = game.getLogic().getAllMoves(p2, game.getBoard());
        assertEquals(52, allMoves.size());
    }

    @Test
    public void allMoves4() {
        Player p1 = new Player().withColor("black").withName("Alice");
        Player p2 = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(p1, p2, Layouts.DEFAULT_TOKEN_LAYOUT);
        ArrayList<Move> allMoves = game.getLogic().getAllMoves(p2, game.getBoard());
        assertEquals(44, allMoves.size());
    }
}
