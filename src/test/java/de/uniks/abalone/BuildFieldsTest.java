package de.uniks.abalone;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.uniks.abalone.model.Board;
import de.uniks.abalone.model.Field;

public class BuildFieldsTest {
    /*-
     * --------------------------------------------
     * -- row = y         dia = x        a[y][x] --
     * --------------------------------------------
     * 0            O O O O O
     * 1           O O O O O O
     * 2          O O O O O O O
     * 3         O O O O O O O O
     * 4        O O O O O O O O O
     * 5         O O O O O O O O 8
     * 6          O O O O O O O 7
     * 7           O O O O O O 6
     * 8            O O O O O 5
     * 0             0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void f83NeighborTest() {
        Board board = new Board();
        board.buildFields();

        board.print2DimArray();
        board.printBoard();

        // find field(8,3)
        Field f83 = null;
        for(Field f: board.getFields()) {

            if(f.getX() == 8 && f.getY() == 3) {
                f83 = f;
            }
        }

        // check neighbors of f83
        assertEquals(7, f83.getLeft().getX());
        assertEquals(3, f83.getLeft().getY());

        assertEquals(null, f83.getRight());

        assertEquals(8, f83.getTopLeft().getX());
        assertEquals(2, f83.getTopLeft().getY());

        assertEquals(8, f83.getDownRight().getX());
        assertEquals(4, f83.getDownRight().getY());

        assertEquals(null, f83.getTopRight());

        assertEquals(7, f83.getDownLeft().getX());
        assertEquals(4, f83.getDownLeft().getY());

    }

    @Test
    public void f80NeighborTest() {
        Board board = new Board();
        board.buildFields();

        board.print2DimArray();
        board.printBoard();

        // find field(8,0)
        Field f80 = null;
        for(Field f: board.getFields()) {

            if(f.getX() == 8 && f.getY() == 0) {
                f80 = f;
            }
        }

        // check neighbors of f80
        assertEquals(7, f80.getLeft().getX());
        assertEquals(0, f80.getLeft().getY());

        assertEquals(null, f80.getRight());

        assertEquals(null, f80.getTopLeft());

        assertEquals(8, f80.getDownRight().getX());
        assertEquals(1, f80.getDownRight().getY());

        assertEquals(null, f80.getTopRight());

        assertEquals(7, f80.getDownLeft().getX());
        assertEquals(1, f80.getDownLeft().getY());

    }

    @Test
    public void f26NeighborTest() {
        Board board = new Board();
        board.buildFields();

        board.print2DimArray();
        board.printBoard();

        // find field(2,6)
        Field f26 = null;
        for(Field f: board.getFields()) {

            if(f.getX() == 2 && f.getY() == 6) {
                f26 = f;
            }
        }

        // check neighbors of f26
        assertEquals(1, f26.getLeft().getX());
        assertEquals(6, f26.getLeft().getY());

        assertEquals(3, f26.getRight().getX());
        assertEquals(6, f26.getRight().getY());

        assertEquals(2, f26.getTopLeft().getX());
        assertEquals(5, f26.getTopLeft().getY());

        assertEquals(2, f26.getDownRight().getX());
        assertEquals(7, f26.getDownRight().getY());

        assertEquals(3, f26.getTopRight().getX());
        assertEquals(5, f26.getTopRight().getY());

        assertEquals(1, f26.getDownLeft().getX());
        assertEquals(7, f26.getDownLeft().getY());
    }
}