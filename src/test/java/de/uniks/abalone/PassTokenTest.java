package de.uniks.abalone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.uniks.abalone.model.Field;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.tools.Layouts;

/**
 * This test verifies the correct functionality of the {@code passField()} method of the game logic. Some of the tests
 * need a little bit of preparation to construct the necessary board situation.
 */
public class PassTokenTest {
    // This is the game object with which all tests will operate
    private Game game;

    /*
     * This method reinitializes the game object before every test. There are two players called Alice and Bob. Bob
     * plays with the white tokens. Alice plays with the black ones and for the tests she is the active player. The game
     * starts with the classic configuration.
     */
    @Before
    public void initTestBench() {
        Player alice = new Player().withName("Alice").withColor("BLACK");
        Player bob = new Player().withName("Bob").withColor("WHITE");
        this.game = new Game();
        this.game.init(alice, bob, Layouts.DEFAULT_TOKEN_LAYOUT);
    }

    @Test
    public void initializeCurrentMove() {
        Field field = this.game.getFields().first();
        assertNull(this.game.getLogic().getCurrentMove());
        this.game.getLogic().passField(field);
        assertNotNull(this.game.getLogic().getCurrentMove());
    }

    @Test
    public void clickAnEmptyField() {
        Field field = this.game.getFields().filterX(4).filterY(4).first();
        assertNull(field.getToken());
        this.game.getLogic().passField(field);
        assertEquals(0, this.game.getLogic().getCurrentMove().getTokens().size());
        assertFalse(this.game.getLogic().isValdidMove());
        assertEquals(-1, this.game.getLogic().getAlignment());
        assertFalse(field.isSelected());
    }

    @Test
    public void selectAnEnemysToken() {
        Field field = this.game.getFields().filterX(3).filterY(7).first();
        assertEquals(this.game.getCurrentPlayer().getNext(), field.getToken().getPlayer());
        this.game.getLogic().passField(field);
        assertEquals(0, this.game.getLogic().getCurrentMove().getTokens().size());
        assertFalse(this.game.getLogic().isValdidMove());
        assertEquals(-1, this.game.getLogic().getAlignment());
        assertFalse(field.isSelected());
    }

    @Test
    public void selectASingleToken() {
        Field field = this.game.getFields().filterX(5).filterY(1).first();
        this.game.getLogic().passField(field);
        assertEquals(1, this.game.getLogic().getCurrentMove().getTokens().size());
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field.getToken()));
        assertTrue(this.game.getLogic().isValdidMove());
        assertEquals(-1, this.game.getLogic().getAlignment());
        assertTrue(field.isSelected());
    }

    @Test
    public void selectAndDeselectASingleToken() {
        Field field = this.game.getFields().filterX(5).filterY(1).first();
        this.game.getLogic().passField(field);   // Select
        this.game.getLogic().passField(field);   // Deselect
        assertEquals(0, this.game.getLogic().getCurrentMove().getTokens().size());
        assertFalse(this.game.getLogic().getCurrentMove().getTokens().contains(field.getToken()));
        assertFalse(this.game.getLogic().isValdidMove());
        assertEquals(-1, this.game.getLogic().getAlignment());
        assertFalse(field.isSelected());
    }

    @Test
    public void selectTwoAdjacentTokens() {
        Field field1 = this.game.getFields().filterX(6).filterY(1).first();
        Field field2 = field1.getTopRight();
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        assertEquals(2, this.game.getLogic().getCurrentMove().getTokens().size());
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field1.getToken()));
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field2.getToken()));
        assertTrue(this.game.getLogic().isValdidMove());
        assertEquals(0, this.game.getLogic().getAlignment());
        assertTrue(field1.isSelected());
        assertTrue(field2.isSelected());
    }

    @Test
    public void selectTwoAdjacentTokensAndDeselectTheFirstOne() {
        Field field1 = this.game.getFields().filterX(6).filterY(1).first();
        Field field2 = field1.getTopRight();
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field1);
        assertEquals(1, this.game.getLogic().getCurrentMove().getTokens().size());
        assertFalse(this.game.getLogic().getCurrentMove().getTokens().contains(field1.getToken()));
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field2.getToken()));
        assertTrue(this.game.getLogic().isValdidMove());
        assertEquals(-1, this.game.getLogic().getAlignment());
        assertFalse(field1.isSelected());
        assertTrue(field2.isSelected());
    }

    @Test
    public void selectTwoAdjacentTokensAndDeselectTheSecondOne() {
        Field field1 = this.game.getFields().filterX(6).filterY(1).first();
        Field field2 = field1.getTopRight();
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field2);
        assertEquals(1, this.game.getLogic().getCurrentMove().getTokens().size());
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field1.getToken()));
        assertFalse(this.game.getLogic().getCurrentMove().getTokens().contains(field2.getToken()));
        assertTrue(this.game.getLogic().isValdidMove());
        assertEquals(-1, this.game.getLogic().getAlignment());
        assertTrue(field1.isSelected());
        assertFalse(field2.isSelected());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         B B B B B
     * 1        B B B B B O
     * 2       O O B B B O B
     * 3      O O O O O O O O
     * 4     O O O O O O O O O
     * 5      O O O O O O O O 8
     * 6       O O W W W O O 7
     * 7        W W W W W W 6
     * 8         W W W W W 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void selectTwoTokensWithAnEmptyFieldInBetween() {
        // Preparation: Move (8, 1) to (8, 2)
        Field target = this.game.getFields().filterX(8).filterY(2).first();
        target.withToken(target.getTopLeft().getToken());
        // The real test
        Field field1 = this.game.getFields().filterX(8).filterY(0).first();
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(target);
        assertEquals(1, this.game.getLogic().getCurrentMove().getTokens().size());
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field1.getToken()));
        assertFalse(this.game.getLogic().getCurrentMove().getTokens().contains(target.getToken()));
        assertTrue(this.game.getLogic().isValdidMove());
        assertEquals(-1, this.game.getLogic().getAlignment());
        assertTrue(field1.isSelected());
        assertFalse(target.isSelected());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         B B B B B
     * 1        B B B B B W
     * 2       O O B B B O B
     * 3      O O O O O O O O
     * 4     O O O O O O O O O
     * 5      O O O O O O O O 8
     * 6       O O W W W O O 7
     * 7        W W W W W O 6
     * 8         W W W W W 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void selectTwoTokensWithAnEnemysTokenInBetween() {
        // Preparation: Move (8, 1) to (8, 2) and (5, 7) to (8, 1)
        Field target = this.game.getFields().filterX(8).filterY(2).first();
        target.withToken(target.getTopLeft().getToken());
        Field enemy = this.game.getFields().filterX(5).filterY(7).first();
        target.getTopLeft().withToken(enemy.getToken());
        // The real test
        Field field1 = this.game.getFields().filterX(8).filterY(0).first();
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(target);
        assertEquals(1, this.game.getLogic().getCurrentMove().getTokens().size());
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field1.getToken()));
        assertFalse(this.game.getLogic().getCurrentMove().getTokens().contains(target.getToken()));
        assertTrue(this.game.getLogic().isValdidMove());
        assertEquals(-1, this.game.getLogic().getAlignment());
        assertTrue(field1.isSelected());
        assertFalse(target.isSelected());
    }

    @Test
    public void selectTwoTokensWithTwoFieldsInBetween() {
        Field field1 = this.game.getFields().filterX(5).filterY(1).first();
        Field field2 = field1.getRight().getRight().getRight();
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        assertEquals(1, this.game.getLogic().getCurrentMove().getTokens().size());
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field1.getToken()));
        assertFalse(this.game.getLogic().getCurrentMove().getTokens().contains(field2.getToken()));
        assertTrue(this.game.getLogic().isValdidMove());
        assertEquals(-1, this.game.getLogic().getAlignment());
        assertTrue(field1.isSelected());
        assertFalse(field2.isSelected());
    }

    @Test
    public void selectTwoUnalignedTokens() {
        Field field1 = this.game.getFields().filterX(5).filterY(1).first();
        Field field2 = field1.getRight().getDownRight();
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        assertEquals(1, this.game.getLogic().getCurrentMove().getTokens().size());
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field1.getToken()));
        assertFalse(this.game.getLogic().getCurrentMove().getTokens().contains(field2.getToken()));
        assertTrue(this.game.getLogic().isValdidMove());
        assertEquals(-1, this.game.getLogic().getAlignment());
        assertTrue(field1.isSelected());
        assertFalse(field2.isSelected());
    }

    @Test
    public void selectThreeAdjacentAndAlignedTokens() {
        Field field1 = this.game.getFields().filterX(4).filterY(2).first();
        Field field2 = field1.getTopRight();
        Field field3 = field2.getTopRight();
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field3);
        assertEquals(3, this.game.getLogic().getCurrentMove().getTokens().size());
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field1.getToken()));
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field2.getToken()));
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field3.getToken()));
        assertTrue(this.game.getLogic().isValdidMove());
        assertEquals(0, this.game.getLogic().getAlignment());
        assertTrue(field1.isSelected());
        assertTrue(field2.isSelected());
        assertTrue(field3.isSelected());
    }

    @Test
    public void selectThreeAdjacentAndAlignedTokensAndDeselectAnOuterOne() {
        Field field1 = this.game.getFields().filterX(4).filterY(2).first();
        Field field2 = field1.getTopRight();
        Field field3 = field2.getTopRight();
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field3);
        this.game.getLogic().passField(field1);
        assertEquals(2, this.game.getLogic().getCurrentMove().getTokens().size());
        assertFalse(this.game.getLogic().getCurrentMove().getTokens().contains(field1.getToken()));
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field2.getToken()));
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field3.getToken()));
        assertTrue(this.game.getLogic().isValdidMove());
        assertEquals(0, this.game.getLogic().getAlignment());
        assertFalse(field1.isSelected());
        assertTrue(field2.isSelected());
        assertTrue(field3.isSelected());
    }

    @Test
    public void selectThreeAdjacentAndAlignedTokensAndDeselectTheMiddleOne() {
        Field field1 = this.game.getFields().filterX(4).filterY(2).first();
        Field field2 = field1.getTopRight();
        Field field3 = field2.getTopRight();
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field3);
        this.game.getLogic().passField(field2);
        assertEquals(2, this.game.getLogic().getCurrentMove().getTokens().size());
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field1.getToken()));
        assertFalse(this.game.getLogic().getCurrentMove().getTokens().contains(field2.getToken()));
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field3.getToken()));
        assertFalse(this.game.getLogic().isValdidMove());
        assertEquals(0, this.game.getLogic().getAlignment());
        assertTrue(field1.isSelected());
        assertFalse(field2.isSelected());
        assertTrue(field3.isSelected());
    }

    @Test
    public void selectTwoNotAdjacentTokensAndThenTheMiddleOne() {
        Field field1 = this.game.getFields().filterX(4).filterY(2).first();
        Field field2 = field1.getTopRight();
        Field field3 = field2.getTopRight();
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field3);
        this.game.getLogic().passField(field2);
        assertEquals(3, this.game.getLogic().getCurrentMove().getTokens().size());
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field1.getToken()));
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field2.getToken()));
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field3.getToken()));
        assertTrue(this.game.getLogic().isValdidMove());
        assertEquals(0, this.game.getLogic().getAlignment());
        assertTrue(field1.isSelected());
        assertTrue(field2.isSelected());
        assertTrue(field3.isSelected());
    }

    @Test
    public void selectTwoNotAdjacentTokensAndThenAThirdOneWhichIsAlignedButNotInTheMiddle() {
        Field field1 = this.game.getFields().filterX(4).filterY(1).first();
        Field field2 = field1.getRight().getRight();
        Field field3 = field2.getRight();
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field3);
        assertEquals(2, this.game.getLogic().getCurrentMove().getTokens().size());
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field1.getToken()));
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field2.getToken()));
        assertFalse(this.game.getLogic().getCurrentMove().getTokens().contains(field3.getToken()));
        assertFalse(this.game.getLogic().isValdidMove());
        assertEquals(1, this.game.getLogic().getAlignment());
        assertTrue(field1.isSelected());
        assertTrue(field2.isSelected());
        assertFalse(field3.isSelected());
    }

    @Test
    public void selectTwoNotAdjacentTokensAndThenAThirdOneWhichIsUnaligned() {
        Field field1 = this.game.getFields().filterX(4).filterY(2).first();
        Field field2 = field1.getTopRight().getTopRight();
        Field field3 = field2.getRight();
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field3);
        assertEquals(2, this.game.getLogic().getCurrentMove().getTokens().size());
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field1.getToken()));
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field2.getToken()));
        assertFalse(this.game.getLogic().getCurrentMove().getTokens().contains(field3.getToken()));
        assertFalse(this.game.getLogic().isValdidMove());
        assertEquals(0, this.game.getLogic().getAlignment());
        assertTrue(field1.isSelected());
        assertTrue(field2.isSelected());
        assertFalse(field3.isSelected());
    }

    @Test
    public void selectTwoAdjacentTokensAndThenAThirdOneWhichIsUnaligned() {
        Field field1 = this.game.getFields().filterX(4).filterY(2).first();
        Field field2 = field1.getTopRight();
        Field field3 = field2.getRight();
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field3);
        assertEquals(2, this.game.getLogic().getCurrentMove().getTokens().size());
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field1.getToken()));
        assertTrue(this.game.getLogic().getCurrentMove().getTokens().contains(field2.getToken()));
        assertFalse(this.game.getLogic().getCurrentMove().getTokens().contains(field3.getToken()));
        assertTrue(this.game.getLogic().isValdidMove());
        assertEquals(0, this.game.getLogic().getAlignment());
        assertTrue(field1.isSelected());
        assertTrue(field2.isSelected());
        assertFalse(field3.isSelected());
    }
}