package de.uniks.abalone;

import java.util.ArrayList;
import java.util.BitSet;

import de.uniks.abalone.model.AIPlayer;
import de.uniks.abalone.model.Board;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Logic;
import de.uniks.abalone.model.Move;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.tools.Layouts;
import de.uniks.ai.AlphaBeta;
import de.uniks.ai.IterativeDeepening;
import de.uniks.ai.MiniMax;

public class RuntimeMeasurementTest {

    // @Test
    public void measureEverything() {
        // result vars
        ArrayList<BitSet> producedBoards = new ArrayList<>();
        ArrayList<Long> measuredTimes = new ArrayList<>();
        // params
        int depth = 3;
        int turns = 2;
        int[][] layout = Layouts.BELGIAN_DAISY_TOKEN_LAYOUT;
        long start;
        long end;
        // -----------------
        // // // Minimax
        // start = System.currentTimeMillis();
        // producedBoards.add(simMiniMax(turns, layout, depth, false));
        // end = System.currentTimeMillis();
        // measuredTimes.add(end - start);
        // //
        // // // Minimax + Transpo
        // start = System.currentTimeMillis();
        // producedBoards.add(simMiniMax(turns, layout, depth, true));
        // end = System.currentTimeMillis();
        // measuredTimes.add(end - start);

        // // AlphaBeta + Vorsortierung
        start = System.currentTimeMillis();
        producedBoards.add(simAlphaBeta(turns, layout, depth, false));
        end = System.currentTimeMillis();
        measuredTimes.add(end - start);

        // AlphaBeta + Transpo + Vorsortierung
        start = System.currentTimeMillis();
        producedBoards.add(simAlphaBeta(turns, layout, depth, true));
        end = System.currentTimeMillis();
        measuredTimes.add(end - start);

        // // Iterative Deepening
        start = System.currentTimeMillis();
        producedBoards.add(simIterativeDeepening(turns, layout, depth, true, false));
        end = System.currentTimeMillis();
        measuredTimes.add(end - start);

        System.out.println("measuredTimes:");
        for(Long time: measuredTimes) {
            System.out.println(time);
        }
        System.out.println("producedBoards:");
        for(BitSet board: producedBoards) {
            System.out.println(board);
        }
    }

    public BitSet simMiniMax(int iterations, int[][] layout, int depth, boolean transpoTableActive) {
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<>();
        mm.setTraTableActive(transpoTableActive);
        game.init(aiPlayer, player, layout);
        while(iterations > 0) {
            Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), depth);
            game.getLogic().executeMove(bestMove);
            bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), depth);
            game.getLogic().executeMove(bestMove);
            iterations--;
        }
        System.out.println("size" + mm.tt.size + " puts: " + mm.tt.total + " collisions:" + mm.tt.collision
                + " entriesUsed: " + mm.entriesUsed);
        return game.getBoard().encode();
    }

    public BitSet simAlphaBeta(int iterations, int[][] layout, int depth, boolean transpoTableActive) {
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        AlphaBeta<Logic, Board, Move, Player> ab = new AlphaBeta<>();
        ab.setTraTableActive(transpoTableActive);
        game.init(aiPlayer, player, layout);
        while(iterations > 0) {
            Move bestMove = ab.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), depth);
            game.getLogic().executeMove(bestMove);
            bestMove = ab.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), depth);
            game.getLogic().executeMove(bestMove);
            iterations--;
        }
        System.out.println("size" + ab.tt.size + " puts: " + ab.tt.total + " collisions:" + ab.tt.collision
                + " entriesUsed: " + ab.entriesUsed);
        return game.getBoard().encode();
    }

    public BitSet simIterativeDeepening(int iterations, int[][] layout, int depth, boolean transpoTableActive,
            boolean sortMovesActive) {
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> id = new IterativeDeepening<>();
        // ab.setTraTableActive(transpoTableActive);
        game.init(aiPlayer, player, layout);
        while(iterations > 0) {
            Move bestMove = id.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), depth);
            game.getLogic().executeMove(bestMove);
            bestMove = id.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), depth);
            game.getLogic().executeMove(bestMove);
            iterations--;
        }
        // System.out.println("size" + ab.tt.size + " puts: " + ab.tt.total + " collisions:" + ab.tt.collision
        // + " entriesUsed: " + ab.entriesUsed);
        return game.getBoard().encode();
    }
}
