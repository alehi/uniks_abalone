package de.uniks.abalone;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.uniks.abalone.model.Field;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Move;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.tools.Layouts;

public class LastMovedTest {
    private Game game;

    @Before
    public void initGame() {
        Player p1 = new Player().withName("Alice").withColor("Blue");
        Player p2 = new Player().withName("Bob").withColor("Yellow");

        this.game = new Game();
        this.game.init(p1, p2, Layouts.DEFAULT_TOKEN_LAYOUT);
    }

    @Test
    public void testSingleMove() {
        Field f1 = this.game.getFields().filterX(4).filterY(2).first();
        Field f2 = f1.getRight();
        Field f3 = f2.getRight();

        this.game.getLogic()
                .executeMove(new Move().withTokens(f1.getToken(), f2.getToken(), f3.getToken()).withDirection(2));

        assertTrue(f1.getDownRight().isLastMoved());
        assertTrue(f2.getDownRight().isLastMoved());
        assertTrue(f3.getDownRight().isLastMoved());
    }

    @Test
    public void testTwoMoves() {
        // Player 1
        Field f1 = this.game.getFields().filterX(4).filterY(2).first();
        Field f2 = f1.getRight();
        Field f3 = f2.getRight();

        // Player 2
        Field f4 = this.game.getFields().filterX(2).filterY(6).first();
        Field f5 = f4.getRight();
        Field f6 = f5.getRight();

        this.game.getLogic()
                .executeMove(new Move().withTokens(f1.getToken(), f2.getToken(), f3.getToken()).withDirection(2));
        this.game.withCurrentPlayer(this.game.getCurrentPlayer().getNext());
        this.game.getLogic()
                .executeMove(new Move().withTokens(f4.getToken(), f5.getToken(), f6.getToken()).withDirection(5));

        assertFalse(f1.getDownRight().isLastMoved());
        assertFalse(f2.getDownRight().isLastMoved());
        assertFalse(f3.getDownRight().isLastMoved());

        assertTrue(f4.getTopLeft().isLastMoved());
        assertTrue(f5.getTopLeft().isLastMoved());
        assertTrue(f6.getTopLeft().isLastMoved());
    }
}