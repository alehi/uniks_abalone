package de.uniks.abalone;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Player;

/**
 * This test verifies the correct functionality of the normalization of the board for {@code Board.encode()}. Different
 * boards will be encoded and every board must result in the same hash value as the given control board.
 */
public class TranspositionTest {
    // The control game which will be used to compare the results of the normalization.
    private static Game controlGame;

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         O O V O O
     * 1        O O V O O O
     * 2       O O O X O V O
     * 3      O V O O O O O O
     * 4     X O O O V O X O O
     * 5      V O O O X O V O 8
     * 6       O O O V O O O 7
     * 7        O O O O V O 6
     * 8         V O X O O 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @BeforeClass
    public static void setUpControllPattern() {
        int[][] layout = {
                {0, 0, 0, 0, 0, 0, 2, 0, 0},
                {0, 0, 0, 0, 0, 2, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 0, 2, 0},
                {0, 0, 2, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 2, 0, 1, 0, 0},
                {2, 0, 0, 0, 1, 0, 2, 0, 0},
                {0, 0, 0, 2, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {2, 0, 1, 0, 0, 0, 0, 0, 0}};

        controlGame = new Game();
        controlGame.init(new Player().withColor("Xenongelb"), new Player().withColor("Volvo-Blau"), layout);
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         O O V O O
     * 1        O O V O O O
     * 2       O O O X O V O
     * 3      O V O O O O O O
     * 4     X O O O V O X O O
     * 5      V O O O X O V O 8
     * 6       O O O V O O O 7
     * 7        O O O O V O 6
     * 8         V O X O O 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void testIdentity() {
        // The board configuration on which the transposition will be used
        int[][] layout = {
                {0, 0, 0, 0, 0, 0, 2, 0, 0},
                {0, 0, 0, 0, 0, 2, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 0, 2, 0},
                {0, 0, 2, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 2, 0, 1, 0, 0},
                {2, 0, 0, 0, 1, 0, 2, 0, 0},
                {0, 0, 0, 2, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {2, 0, 1, 0, 0, 0, 0, 0, 0}};
        Game testGame = new Game();
        testGame.init(new Player().withColor("Xenongelb"), new Player().withColor("Volvo-Blau"), layout);

        // Both boards should have the same hash value.
        assertEquals(controlGame.getBoard().encode(), testGame.getBoard().encode());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         O O V O O
     * 1        O O O V O O
     * 2       O V O X O O O
     * 3      O O O O O O V O
     * 4     O O X O V O O O X
     * 5      O V O X O O O V 8
     * 6       O O O V O O O 7
     * 7        O V O O O O 6
     * 8         O O X O V 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void testSimpleMirroring() {
        // The board configuration on which the transposition will be used
        int[][] layout = {
                {0, 0, 0, 0, 0, 0, 2, 0, 0},
                {0, 0, 0, 0, 0, 0, 2, 0, 0},
                {0, 0, 0, 2, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 2, 0},
                {0, 0, 1, 0, 2, 0, 0, 0, 1},
                {0, 2, 0, 1, 0, 0, 0, 2, 0},
                {0, 0, 0, 2, 0, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 2, 0, 0, 0, 0}};
        Game testGame = new Game();
        testGame.init(new Player().withColor("Xenongelb"), new Player().withColor("Volvo-Blau"), layout);

        // Both boards should have the same hash value.
        assertEquals(controlGame.getBoard().encode(), testGame.getBoard().encode());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         O O O O O
     * 1        O O V O O O
     * 2       V O O O X V O
     * 3      O V X O O O O O
     * 4     O O O O V X O V O
     * 5      O O O O O V O O 8
     * 6       O V O O O O X 7
     * 7        O O O O O O 6
     * 8         X V O O V 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void testRotateOneSector() {
        // The board configuration on which the transposition will be used
        int[][] layout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 2, 0, 0, 0},
                {0, 0, 2, 0, 0, 0, 1, 2, 0},
                {0, 0, 2, 1, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 1, 0, 2, 0},
                {0, 0, 0, 0, 0, 2, 0, 0, 0},
                {0, 2, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {1, 2, 0, 0, 2, 0, 0, 0, 0}};
        Game testGame = new Game();
        testGame.init(new Player().withColor("Xenongelb"), new Player().withColor("Volvo-Blau"), layout);

        // Both boards should have the same hash value.
        assertEquals(controlGame.getBoard().encode(), testGame.getBoard().encode());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         O O O O O
     * 1        O O O V O O
     * 2       O V X O O O V
     * 3      O O O O O X V O
     * 4     O V O X V O O O O
     * 5      O O V O O O O O 8
     * 6       X O O O O V O 7
     * 7        O O O O O O 6
     * 8         V O O V X 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void testRotateOneSectorAndMirroring() {
        // The board configuration on which the transposition will be used
        int[][] layout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 2, 0, 0},
                {0, 0, 0, 2, 1, 0, 0, 0, 2},
                {0, 0, 0, 0, 0, 0, 1, 2, 0},
                {0, 2, 0, 1, 2, 0, 0, 0, 0},
                {0, 0, 2, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 2, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {2, 0, 0, 2, 1, 0, 0, 0, 0}};
        Game testGame = new Game();
        testGame.init(new Player().withColor("Xenongelb"), new Player().withColor("Volvo-Blau"), layout);

        // Both boards should have the same hash value.
        assertEquals(controlGame.getBoard().encode(), testGame.getBoard().encode());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         O O O O O
     * 1        O O V O V O
     * 2       O O X O O O X
     * 3      O V O O X V O O
     * 4     O O O O V O O O V
     * 5      O O X O O O O O 8
     * 6       V V O O O O O 7
     * 7        O O O V O V 6
     * 8         O O O O X 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void testRotateTwoSectors() {
        // The board configuration on which the transposition will be used
        int[][] layout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 2, 0, 2, 0},
                {0, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 0, 2, 0, 0, 1, 2, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 2},
                {0, 0, 1, 0, 0, 0, 0, 0, 0},
                {2, 2, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 2, 0, 2, 0, 0, 0},
                {0, 0, 0, 0, 1, 0, 0, 0, 0}};
        Game testGame = new Game();
        testGame.init(new Player().withColor("Xenongelb"), new Player().withColor("Volvo-Blau"), layout);

        // Both boards should have the same hash value.
        assertEquals(controlGame.getBoard().encode(), testGame.getBoard().encode());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         O O O O O
     * 1        O V O V O O
     * 2       X O O O X O O
     * 3      O O V X O O V O
     * 4     V O O O V O O O O
     * 5      O O O O O X O O 8
     * 6       O O O O O V V 7
     * 7        V O V O O O 6
     * 8         X O O O O 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void testRotateTwoSectorsAndMirroring() {
        // The board configuration on which the transposition will be used
        int[][] layout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 2, 0, 0},
                {0, 0, 1, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 2, 1, 0, 0, 2, 0},
                {2, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 2, 2, 0, 0},
                {2, 0, 2, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0, 0}};
        Game testGame = new Game();
        testGame.init(new Player().withColor("Xenongelb"), new Player().withColor("Volvo-Blau"), layout);

        // Both boards should have the same hash value.
        assertEquals(controlGame.getBoard().encode(), testGame.getBoard().encode());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         O O X O V
     * 1        O V O O O O
     * 2       O O O V O O O
     * 3      O V O X O O O V
     * 4     O O X O V O O O X
     * 5      O O O O O O V O 8
     * 6       O V O X O O O 7
     * 7        O O O V O O 6
     * 8         O O V O O 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void testRotateThreeSectors() {
        // The board configuration on which the transposition will be used
        int[][] layout = {
                {0, 0, 0, 0, 0, 0, 1, 0, 2},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 2, 0, 0, 0},
                {0, 0, 2, 0, 1, 0, 0, 0, 2},
                {0, 0, 1, 0, 2, 0, 0, 0, 1},
                {0, 0, 0, 0, 0, 0, 2, 0, 0},
                {0, 2, 0, 1, 0, 0, 0, 0, 0},
                {0, 0, 0, 2, 0, 0, 0, 0, 0},
                {0, 0, 2, 0, 0, 0, 0, 0, 0}};
        Game testGame = new Game();
        testGame.init(new Player().withColor("Xenongelb"), new Player().withColor("Volvo-Blau"), layout);

        // Both boards should have the same hash value.
        assertEquals(controlGame.getBoard().encode(), testGame.getBoard().encode());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         V O X O O
     * 1        O O O O V O
     * 2       O O O V O O O
     * 3      V O O O X O V O
     * 4     X O O O V O X O O
     * 5      O V O O O O O O 8
     * 6       O O O X O V O 7
     * 7        O O V O O O 6
     * 8         O O V O O 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void testRotateThreeSectorsAndMirroring() {
        // The board configuration on which the transposition will be used
        int[][] layout = {
                {0, 0, 0, 0, 2, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 2, 0},
                {0, 0, 0, 0, 0, 2, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 0, 2, 0},
                {1, 0, 0, 0, 2, 0, 1, 0, 0},
                {0, 2, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 1, 0, 2, 0, 0, 0},
                {0, 0, 2, 0, 0, 0, 0, 0, 0},
                {0, 0, 2, 0, 0, 0, 0, 0, 0}};
        Game testGame = new Game();
        testGame.init(new Player().withColor("Xenongelb"), new Player().withColor("Volvo-Blau"), layout);

        // Both boards should have the same hash value.
        assertEquals(controlGame.getBoard().encode(), testGame.getBoard().encode());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         V O O V X
     * 1        O O O O O O
     * 2       X O O O O V O
     * 3      O O V O O O O O
     * 4     O V O X V O O O O
     * 5      O O O O O X V O 8
     * 6       O V X O O O V 7
     * 7        O O O V O O 6
     * 8         O O O O O 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void testRotateFourSectors() {
        // The board configuration on which the transposition will be used
        int[][] layout = {
                {0, 0, 0, 0, 2, 0, 0, 2, 1},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 0, 2, 0},
                {0, 0, 0, 2, 0, 0, 0, 0, 0},
                {0, 2, 0, 1, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 2, 0, 0},
                {0, 2, 1, 0, 0, 0, 2, 0, 0},
                {0, 0, 0, 2, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        Game testGame = new Game();
        testGame.init(new Player().withColor("Xenongelb"), new Player().withColor("Volvo-Blau"), layout);

        // Both boards should have the same hash value.
        assertEquals(controlGame.getBoard().encode(), testGame.getBoard().encode());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         X V O O V
     * 1        O O O O O O
     * 2       O V O O O O X
     * 3      O O O O O V O O
     * 4     O O O O V X O V O
     * 5      O V X O O O O O 8
     * 6       V O O O X V O 7
     * 7        O O V O O O 6
     * 8         O O O O O 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void testRotateFourSectorsAndMirroring() {
        // The board configuration on which the transposition will be used
        int[][] layout = {
                {0, 0, 0, 0, 1, 2, 0, 0, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 2, 0, 0, 0, 0, 1},
                {0, 0, 0, 0, 0, 0, 2, 0, 0},
                {0, 0, 0, 0, 2, 1, 0, 2, 0},
                {0, 2, 1, 0, 0, 0, 0, 0, 0},
                {2, 0, 0, 0, 1, 2, 0, 0, 0},
                {0, 0, 2, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        Game testGame = new Game();
        testGame.init(new Player().withColor("Xenongelb"), new Player().withColor("Volvo-Blau"), layout);

        // Both boards should have the same hash value.
        assertEquals(controlGame.getBoard().encode(), testGame.getBoard().encode());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         X O O O O
     * 1        V O V O O O
     * 2       O O O O O V V
     * 3      O O O O O X O O
     * 4     V O O O V O O O O
     * 5      O O V X O O V O 8
     * 6       X O O O X O O 7
     * 7        O V O V O O 6
     * 8         O O O O O 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void testRotateFiveSectors() {
        // The board configuration on which the transposition will be used
        int[][] layout = {
                {0, 0, 0, 0, 1, 0, 0, 0, 0},
                {0, 0, 0, 2, 0, 2, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 2, 2},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {2, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 2, 1, 0, 0, 2, 0, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 0},
                {0, 2, 0, 2, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        Game testGame = new Game();
        testGame.init(new Player().withColor("Xenongelb"), new Player().withColor("Volvo-Blau"), layout);

        // Both boards should have the same hash value.
        assertEquals(controlGame.getBoard().encode(), testGame.getBoard().encode());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         O O O O X
     * 1        O O O V O V
     * 2       V V O O O O O
     * 3      O O X O O O O O
     * 4     O O O O V O O O V
     * 5      O V O O X V O O 8
     * 6       O O X O O O X 7
     * 7        O O V O V O 6
     * 8         O O O O O 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void testRotateFiveSectorsAndMirroring() {
        // The board configuration on which the transposition will be used
        int[][] layout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 1},
                {0, 0, 0, 0, 0, 0, 2, 0, 2},
                {0, 0, 2, 2, 0, 0, 0, 0, 0},
                {0, 0, 0, 1, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 2},
                {0, 2, 0, 0, 1, 2, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 1, 0, 0},
                {0, 0, 2, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        Game testGame = new Game();
        testGame.init(new Player().withColor("Xenongelb"), new Player().withColor("Volvo-Blau"), layout);

        // Both boards should have the same hash value.
        assertEquals(controlGame.getBoard().encode(), testGame.getBoard().encode());
    }
}