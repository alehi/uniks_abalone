package de.uniks.abalone;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.uniks.abalone.model.Field;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Move;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.model.Token;

public class ExecuteMoveTest {

    @Test
    public void moveSingleTokenTopLeft() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O O O O
        4     O O O O $ O O O O
        5      O O O O W O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        Player p1 = new Player().withColor("black").withName("Alice");
        Player p2 = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(p1, p2, tokenLayout);

        Token token = p2.getTokens().get(0);
        Move move = new Move().withDirection(Move.TOP_LEFT).withTokens(token);
        Field target = token.getField().getTopLeft();

        // action
        game.getLogic().executeMove(move);
        // -----
        game.getBoard().printBoard();
        assertEquals(target, token.getField());
    }

    @Test
    public void moveSingleTokenTopRight() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O O O O
        4     O O O O O $ O O O
        5      O O O O W O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        Player p1 = new Player().withColor("black").withName("Alice");
        Player p2 = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(p1, p2, tokenLayout);

        Token token = p2.getTokens().get(0);
        Move move = new Move().withDirection(Move.TOP_RIGHT).withTokens(token);
        Field target = token.getField().getTopRight();

        // action
        game.getLogic().executeMove(move);
        // -----
        game.getBoard().printBoard();
        assertEquals(target, token.getField());
    }

    @Test
    public void moveSingleTokenOutOfTheBoardEqualsSuicide() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O $
        3      O O O O O O O W
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        Player p1 = new Player().withColor("black").withName("Alice");
        Player p2 = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(p1, p2, tokenLayout);

        Token token = p2.getTokens().get(0);
        Move move = new Move().withDirection(Move.TOP_RIGHT).withTokens(token);
        Field target = token.getField().getTopRight();

        // action
        game.getLogic().executeMove(move);
        // -----
        game.getBoard().printBoard();
        assertEquals(null, token.getField());
        assertEquals(target, token.getField());
    }

    @Test
    public void moveThreeTokenDiagonalTopRight() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O $ O O O
        3      O O O W $ O O O
        4     O O O O W $ O O O
        5      O O O O W O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        Player p1 = new Player().withColor("black").withName("Alice");
        Player p2 = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(p1, p2, tokenLayout);

        Token token = p2.getTokens().get(0);
        Token token2 = p2.getTokens().get(1);
        Token token3 = p2.getTokens().get(2);
        Move move = new Move().withDirection(Move.TOP_RIGHT).withTokens(token, token2, token3);
        Field target = token.getField().getTopRight();
        Field target2 = token2.getField().getTopRight();
        Field target3 = token3.getField().getTopRight();

        // action
        game.getLogic().executeMove(move);
        // -----
        game.getBoard().printBoard();
        assertEquals(target, token.getField());
        assertEquals(target2, token2.getField());
        assertEquals(target3, token3.getField());
    }

    @Test
    public void moveThreeTokenInALineTopLeft() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O $ O O O O
        3      O O O W O O O O
        4     O O O O W O O O O
        5      O O O O W O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        Player p1 = new Player().withColor("black").withName("Alice");
        Player p2 = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(p1, p2, tokenLayout);

        Token token = p2.getTokens().get(0);
        Token token2 = p2.getTokens().get(1);
        Token token3 = p2.getTokens().get(2);
        Move move = new Move().withDirection(Move.TOP_LEFT).withTokens(token, token2, token3);
        Field target = token.getField().getTopLeft();
        Field target2 = token2.getField().getTopLeft();
        Field target3 = token3.getField().getTopLeft();

        // action
        game.getLogic().executeMove(move);
        // -----
        game.getBoard().printBoard();
        assertEquals(target, token.getField());
        assertEquals(target2, token2.getField());
        assertEquals(target3, token3.getField());
    }

    @Test
    public void moveThreeTokenInALineDownRight() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O W O O O O
        4     O O O O W O O O O
        5      O O O O W O O O 8
        6       O O O O $ O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        Player p1 = new Player().withColor("black").withName("Alice");
        Player p2 = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(p1, p2, tokenLayout);

        Token token = p2.getTokens().get(0);
        Token token2 = p2.getTokens().get(1);
        Token token3 = p2.getTokens().get(2);
        Move move = new Move().withDirection(Move.DOWN_RIGHT).withTokens(token, token2, token3);
        Field target = token.getField().getDownRight();
        Field target2 = token2.getField().getDownRight();
        Field target3 = token3.getField().getDownRight();

        // action
        game.getLogic().executeMove(move);
        // -----
        game.getBoard().printBoard();
        assertEquals(target, token.getField());
        assertEquals(target2, token2.getField());
        assertEquals(target3, token3.getField());
    }

    @Test
    public void moveThreeTokenInALineTopLeftAndPushOneEnemyToken() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O $ O O O O
        2       O O B O O O O
        3      O O O W O O O O
        4     O O O O W O O O O
        5      O O O O W O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 1, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        Player p1 = new Player().withColor("black").withName("Alice");
        Player p2 = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(p1, p2, tokenLayout);

        Token token = p2.getTokens().get(0);
        Token token2 = p2.getTokens().get(1);
        Token token3 = p2.getTokens().get(2);
        Token eToken = p1.getTokens().get(0);
        Move move = new Move().withDirection(Move.TOP_LEFT).withTokens(token, token2, token3)
                .withOpponentTokens(eToken);
        Field target = token.getField().getTopLeft();
        Field target2 = token2.getField().getTopLeft();
        Field target3 = token3.getField().getTopLeft();
        Field eTarget = eToken.getField().getTopLeft();

        // action
        game.getLogic().executeMove(move);
        // -----
        game.getBoard().printBoard();
        assertEquals(target, token.getField());
        assertEquals(target2, token2.getField());
        assertEquals(target3, token3.getField());
        assertEquals(eTarget, eToken.getField());
    }

    @Test
    public void moveThreeTokenInALineTopLeftAndPushTwoEnemyToken() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         $ O O O O
        1        O B O O O O
        2       O O B O O O O
        3      O O O W O O O O
        4     O O O O W O O O O
        5      O O O O W O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 1, 0, 0, 0, 0},
                {0, 0, 0, 0, 1, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        Player p1 = new Player().withColor("black").withName("Alice");
        Player p2 = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(p1, p2, tokenLayout);

        Token token = p2.getTokens().get(0);
        Token token2 = p2.getTokens().get(1);
        Token token3 = p2.getTokens().get(2);
        Token eToken = p1.getTokens().get(0);
        Token eToken2 = p1.getTokens().get(1);
        Move move = new Move().withDirection(Move.TOP_LEFT).withTokens(token, token2, token3).withOpponentTokens(eToken,
                eToken2);
        Field target = token.getField().getTopLeft();
        Field target2 = token2.getField().getTopLeft();
        Field target3 = token3.getField().getTopLeft();
        Field eTarget = eToken.getField().getTopLeft();
        Field eTarget2 = eToken2.getField().getTopLeft();

        // action
        game.getLogic().executeMove(move);
        // -----
        game.getBoard().printBoard();
        assertEquals(target, token.getField());
        assertEquals(target2, token2.getField());
        assertEquals(target3, token3.getField());
        assertEquals(eTarget, eToken.getField());
        assertEquals(eTarget2, eToken2.getField());
    }

    @Test
    public void moveThreeTokenInALineToTheRightAndPushOneEnemyTokenOutOfTheBoard() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O W W W B B $
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 2, 2, 1, 1},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        Player p1 = new Player().withColor("black").withName("Alice");
        Player p2 = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(p1, p2, tokenLayout);

        Token token = p2.getTokens().get(0);
        Token token2 = p2.getTokens().get(1);
        Token token3 = p2.getTokens().get(2);
        Token eToken = p1.getTokens().get(0);
        Token eToken2 = p1.getTokens().get(1);
        Move move = new Move().withDirection(Move.RIGHT).withTokens(token, token2, token3).withOpponentTokens(eToken,
                eToken2);
        Field target = token.getField().getRight();
        Field target2 = token2.getField().getRight();
        Field target3 = token3.getField().getRight();
        Field eTarget = eToken.getField().getRight();
        Field eTarget2 = eToken2.getField().getRight();

        // action
        game.getLogic().executeMove(move);
        // -----
        game.getBoard().printBoard();
        assertEquals(target, token.getField());
        assertEquals(target2, token2.getField());
        assertEquals(target3, token3.getField());
        assertEquals(eTarget, eToken.getField());
        assertEquals(eTarget2, eToken2.getField());
        assertEquals(null, eToken2.getField());
    }

}
