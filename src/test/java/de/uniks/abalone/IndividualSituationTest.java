package de.uniks.abalone;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Player;

/**
 * This test provides the functionality of some kind of board editor. In the {@code situation} array you can define your
 * initial situation. Afterwards you can create the necessary players by commenting in and out the regarding lines. If
 * you need player 2 as the current player you have to comment in line 51.
 * <p>
 * Now you can set up your test in {@code testSomething()} and test whatever you want to.
 */
public class IndividualSituationTest {
    private Game game;

    @Before
    public void initGame() {
        // ===== The game =====
        this.game = new Game();

        // ===== The board configuration =====
        int[][] situation = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 1, 1, 1, 2, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};

        // ===== The players (choose player type and AI mode) =====
        Player player1 = new Player().withName("Alice").withColor("WHITE");
        // int strength1 = 4;
        // Player player1 = new AIPlayer().withStrength(strength1).withName("Alice").withColor("WHITE");
        // AI<Logic, Board, Move, Player> ai1 = new MiniMax<>();
        // AI<Logic, Board, Move, Player> ai1 = new AlphaBeta<>();
        // AI<Logic, Board, Move, Player> ai1 = new IterativeDeepening<>();
        // ((IterativeDeepening<Logic, Board, Move, Player>) ai1).setTimeLimit(1000 * strength1);
        // ((AIPlayer) player1).setAi(ai1);

        Player player2 = new Player().withName("Bob").withColor("BLACK");
        // int strength2 = 4;
        // Player player2 = new AIPlayer().withStrength(strength2).withName("Bob").withColor("BLACK");
        // AI<Logic, Board, Move, Player> ai2 = new MiniMax<>();
        // AI<Logic, Board, Move, Player> ai2 = new AlphaBeta<>();
        // AI<Logic, Board, Move, Player> ai1 = new IterativeDeepening<>();
        // ((IterativeDeepening<Logic, Board, Move, Player>) ai1).setTimeLimit(1000 * strength2);
        // ((AIPlayer) player2).setAi(ai2);

        // ===== Initialize the game =====
        this.game.init(player1, player2, situation);

        // ===== Change the current player to player 2 if necessary
        // game.withCurrentPlayer(player2);

        this.game.getBoard().printBoard();
    }

    @Test
    public void testSomething() {
        assertTrue(true);
    }
}